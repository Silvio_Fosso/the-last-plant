//
//  Prologo.swift
//  game
//
//  Created by Silvio Fosso on 23/11/2019.
//  Copyright © 2019 Silvio Fosso. All rights reserved.
//

import Foundation
import GameplayKit
import CoreMotion
class Prologo: SKScene,SKPhysicsContactDelegate {
    var Schermata = SKSpriteNode()
    var btn = SKSpriteNode()
    var cont = 0
    var messo = false
    var btnBack = SKSpriteNode()
    var btnSkip = SKSpriteNode()
    var nomi = ["prima","seconda","terza","quarta","ultima"]
    override func didMove(to view: SKView) {
       let text = SKTexture(image: UIImage(named: nomi[cont] )!)
        Schermata = SKSpriteNode(texture: text,size: text.size())
        Schermata.alpha = 0
         
        Schermata.position = CGPoint(x: 0, y: 0)
        addChild(Schermata)
        let textbtn = SKTexture(image: UIImage(named: "Avanti")!)
        btn = SKSpriteNode(texture: textbtn,size: textbtn.size())
        btn.alpha = 0
        let textbtnBack = SKTexture(image: UIImage(named: "Indietro")!)
        btnBack = SKSpriteNode(texture: textbtnBack,size: textbtnBack.size())
        btnBack.zPosition = 3
        btnBack.position = CGPoint(x: -800, y: 0)
        btnBack.alpha = 0
        btnBack.name = "Indietro"
        addChild(btn)
        Schermata.zPosition = 2
        btn.position = CGPoint(x: 800, y: 0)
        btn.zPosition = 3
        btn.name = "Avanti"
        btnSkip.zPosition = 1
        
        let Texture = SKTexture(image: UIImage(named: "Skip")!)
        btnSkip = SKSpriteNode(texture: Texture, size: Texture.size())
        btnSkip.position = CGPoint(x: 800, y: -400)
        btnSkip.alpha = 0
       addChild(btnSkip)
       
        btnSkip.name="Skip"
        btnSkip.zPosition = 4
        var Fade = SKAction.fadeAlpha(to: 1.0, duration: 0.5)
        Schermata.run(Fade, completion: {
            Fade = SKAction.fadeAlpha(to: 1.0, duration: 1.5)
            self.btn.run(Fade)
            self.btnSkip.run(Fade)
        })
         
        
       
       
        
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
                   let location = touch.location(in: self)
                   let node : SKNode = self.atPoint(location)
                   if node.name == "Avanti" {
                      cambiaImMagineAvanti ()
                   }
            if node.name == "Indietro"
            {
               CambiaImmagineIndietro()
            }
            if node.name == "Skip"
            {
                let scene = GameScene(fileNamed: "GameScene")!
                scene.scaleMode = .aspectFill
                let transition = SKTransition.moveIn(with: .right, duration: 1)
                self.view?.presentScene(scene, transition: transition)
            }
               }
    }
    
    
    func CambiaImmagineIndietro()
    {
        let Fad = SKAction.fadeAlpha(to: 0, duration: 1.0)
        let FadIn = SKAction.fadeAlpha(to: 1, duration: 0.5)
        let FadInBtn = SKAction.fadeAlpha(to: 1, duration: 2.0)
        if cont-1 != 0{
            btn.run(Fad)
            btnBack.run(Fad)
            btnSkip.run(Fad)
            Schermata.run(Fad, completion: {
                if(self.cont == 4)
            {
                self.btn.texture = SKTexture(image: UIImage(named: "Avanti")!);
                }
            self.cont-=1
            self.Schermata.texture = SKTexture(image: UIImage(named: self.nomi[self.cont] )!);        self.Schermata.run(FadIn)
                self.btn.run(FadInBtn)
                self.btnBack.run(FadInBtn)
                 self.btnSkip.run(FadInBtn)
            })
        }
        if (cont-1 == 0)
        {
            btnBack.removeAllActions()
            btn.run(Fad)
            btnBack.run(Fad)
            btnSkip.run(Fad)
            Schermata.run(Fad, completion: {
            self.cont-=1
            self.Schermata.texture = SKTexture(image: UIImage(named: self.nomi[self.cont] )!);
                self.btnSkip.run(FadInBtn)
                self.Schermata.run(FadIn)
                self.btn.run(FadInBtn)
//                self.btnBack.run(FadInBtn)
                   
            })
            
            
        }
    }
    
    
    
    
    func cambiaImMagineAvanti ()
    {
        let Fad = SKAction.fadeAlpha(to: 0, duration: 1.0)
        let FadIn = SKAction.fadeAlpha(to: 1, duration: 0.5)
        let FadInBtn = SKAction.fadeAlpha(to: 1, duration: 2.0)
        
        if (cont == 0)
        {
            btn.run(Fad)
            btnBack.alpha = 0
            if messo == false{
                addChild(btnBack)
                messo = true
            }
            btnSkip.run(Fad)
            Schermata.run(Fad, completion: {
                self.cont+=1
                self.Schermata.texture = SKTexture(image: UIImage(named: self.nomi[self.cont] )!);
                self.Schermata.run(FadIn)
                self.btn.run(FadInBtn)
                self.btnBack.run(FadInBtn)
                self.btnSkip.run(FadInBtn)
            })
        } else if (cont < 4)
        {
            btn.run(Fad)
            btnBack.run(Fad)
            btnSkip.run(Fad)
            Schermata.run(Fad, completion: {
            self.cont+=1
            self.Schermata.texture = SKTexture(image: UIImage(named: self.nomi[self.cont] )!);
                if(self.cont == 4)
                {
                 let text = SKTexture(image: UIImage(named: "AvantiGo" )!)
                   self.btn.texture = SKTexture(image: UIImage(named: "AvantiGo" )!)
                    self.btn.size = text.size()
                }
                self.Schermata.run(FadIn)
                self.btn.run(FadInBtn)
                self.btnBack.run(FadInBtn)
                 self.btnSkip.run(FadInBtn)
                           
            })
        } else if (cont == 4)
        {
            let scene = GameScene(fileNamed: "GameScene")!
            scene.scaleMode = .aspectFill
            let transition = SKTransition.moveIn(with: .right, duration: 1)
            self.view?.presentScene(scene, transition: transition)
        }
        
    }
}
