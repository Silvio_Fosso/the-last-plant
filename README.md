# The Last Plant

This app is written in swift on xcode

We're in 2068. Grace, our young heroine, is one of the members of the protest movement against the current system.


After several demonstration acts with her group, she decides to give an extreme demonstration: entering the laboratories, arriving at the production center and stealing the secret of the last plant. 

During the game the player is in the basement and the theft is already accomplished. He has to help Grace escape. 

His mission includes clarify the corporation's history by recovering the documents that can lead to its end.



![](video/1.gif)
![](video/1.mov)
