
//
//  GameScene.swift
//  game
//
//  Created by Christian Mileto on 23/11/2019.
//  Copyright © 2019 Christian Mileto. All rights reserved.
//

import SpriteKit
import GameplayKit
import CoreMotion
import AVFoundation
class Livello2: SKScene,SKPhysicsContactDelegate {

    
    var PlayerArtefatti = AVAudioPlayer()
    var PlayerTaser = AVAudioPlayer()
    var PlayerEhi = AVAudioPlayer()
    var PlayerNemicoCorsa = AVAudioPlayer()
    var PlayerBackground = AVAudioPlayer()
    var PlayerEnd = AVAudioPlayer()

    var motionManager = CMMotionManager()
    var shake = 0
    var taser = SKSpriteNode()
    let blueBallCategory : UInt32 = 0x1 << 0
      let orangeBallCategory : UInt32 = 0x1 << 1
    let pavimento2 : UInt32 = 0x1 << 2
    let pavimentose : UInt32 = 0x1 << 7
    let nemic : UInt32 = 0x1 << 4
    let asc : UInt32 = 0x1 << 3
    let taser1 : UInt32 = 0x1 << 5
    let pavimentosott : UInt32 = 0x1 << 6
    let pavimentoterz : UInt32 = 0x1 << 8
    var move = false
    var shape1 = SKSpriteNode()
    var secondop = SKSpriteNode()
    var a = 0
    var b = 0
    var c = 0
    var d = 0
    var mortenemico = false
    var nemico = SKSpriteNode()
    var loc = CGPoint()
    var btn = UIButton()
    var ground2 = SKSpriteNode()
    var direzione : Bool = false
    var btnpressed : Bool = false
    var changeTexture = SKAction()
    var moveeni = false
    var contatto = false
    var dead = false
    var taserfunz = false
    var ascens = SKSpriteNode()
    var e = 0
    var vita = 3
    var ancoraflag = 0
    var metti = 0
    var timea = Timer()
    var timea1 = Timer()
    var pavimentoSotto = SKSpriteNode()
    var nemico2 = SKSpriteNode()
    var nemico3 = SKSpriteNode()
    var nemico4 = SKSpriteNode()
    var contatto2 = false
    var contatto3 = false
    var contatto4 = false
    var mortenemico4 = false
    var mortenemico3 = false
    var mortenemico2 = false
    var g = 0
    var h = 0
    var k = 0
    var mantienitext = 0
    var mantienitext2 = 0
    var mantienitext3 = 0
    var mantienitext4 = 0
    var salvaG = 0
    var terzop = SKSpriteNode()
     var j = 0
    var sound = true
     var cuore1 = SKSpriteNode()
    var timer1 = Timer()
     var timer2 = Timer()
    var timer3 = Timer()  // timer 1
     var timer4 = Timer() // timer 2
     var cuore2 = SKSpriteNode()
     var fulmine = SKSpriteNode()
     var cuore3 = SKSpriteNode()
     var artefatto1 = SKSpriteNode()
     var artefatto2 = SKSpriteNode()
     var artefatto3 = SKSpriteNode()
     var artefatto4 = SKSpriteNode()
     var artefatto5 = SKSpriteNode()
     var prendiArtEPianoSotto = false
     var prendiArtPiano1 = false
     var prendiArtPiano2 = false
     var prendiArtPiano3 = false
         var sirena = SKSpriteNode()
    var quellosopra = false
    var mettiSire = false
    var stordito = 0
    var stordito2 = 0
    var stordito3 = 0
     var stordito4 = 0
    var timer10 = Timer()
    var timer20 = Timer()
    var timer30 = Timer()
    var timer40 = Timer()
    var portaFalsa = SKSpriteNode()
    var GameOver = false
    override func sceneDidLoad() {
           let path2 = Bundle.main.path(forResource: "a.flat", ofType:nil)!
                                            let url2 = URL(fileURLWithPath: path2)

                                            do {
                                              
                                                self.PlayerBackground = try AVAudioPlayer(contentsOf: url2)
                                                self.PlayerBackground.play()
                                              }
                                             catch {
                                               print("nada")
                                            }
        
    }
    override func didMove(to view: SKView) {

           
           let textarteff = SKTexture(image: UIImage(named: "Tav")!)
           artefatto1 = SKSpriteNode(texture: textarteff)
           artefatto1.position = CGPoint(x: -900, y: -400)
           artefatto1.name = "art"
           artefatto1.size = CGSize(width: 100, height: 100)
           artefatto1.zPosition = 2
           addChild(artefatto1)
           
            artefatto2 = SKSpriteNode(texture: textarteff)
           artefatto2.position = CGPoint(x: -600, y:-100)
           artefatto2.name = "art2"
           artefatto2.size = CGSize(width: 100, height: 100)
           artefatto2.zPosition = 2
           addChild(artefatto2)
           
           
           artefatto3 = SKSpriteNode(texture: textarteff)
           artefatto3.position = CGPoint(x: 700, y: 40)
           artefatto3.name = "art3"
           artefatto3.size = CGSize(width: 100, height: 100)
           artefatto3.zPosition = 3
           addChild(artefatto3)
           
           
          
           
           artefatto4 = SKSpriteNode(texture: textarteff)
           artefatto4.position = CGPoint(x: -800, y: 320)
           artefatto4.name = "art4"
           artefatto4.size = CGSize(width: 100, height: 100)
           artefatto4.zPosition = 3
           addChild(artefatto4)
           
           
           
        
           
           let cuoreText = SKTexture(image: UIImage(named: "cuorePieno")!)
           cuore1 = SKSpriteNode(texture: cuoreText, size: CGSize(width: 30, height: 30))
           cuore1.position = CGPoint(x: 1000, y: -522)
           cuore2 = SKSpriteNode(texture: cuoreText, size: CGSize(width: 30, height: 30))
           cuore2.position = CGPoint(x: 1040, y: -522)
           cuore3 = SKSpriteNode(texture: cuoreText, size: CGSize(width: 30, height: 30))
           cuore3.position = CGPoint(x: 1090, y: -522)
           addChild(cuore1)
           addChild(cuore2)
           addChild(cuore3)
           cuore1.zPosition = 5
           cuore2.zPosition=5
           cuore3.zPosition = 5
           
        
          
           
          
        
           
           let fulminetex = SKTexture(image: UIImage(named: "FulminePieno")!)
           fulmine = SKSpriteNode(texture: fulminetex, size: CGSize(width: 80, height: 60))
           fulmine.position = CGPoint(x: 1130, y: -510)
           addChild(fulmine)
           fulmine.zPosition = 5
           
           
           let murosin = SKSpriteNode()
           murosin.size = CGSize(width: 10, height: 1000)
           murosin.position = CGPoint(x: -1200, y: 0)
           addChild(murosin)
           murosin.physicsBody = SKPhysicsBody(rectangleOf: murosin.size)
           murosin.physicsBody?.affectedByGravity = false
           murosin.physicsBody?.isDynamic = false
           murosin.zPosition = 2
           
           
        
           
           let murodes = SKSpriteNode()
           murodes.size = CGSize(width: 10, height: 1000)
           murodes.position = CGPoint(x: 1200, y: 0)
           addChild(murodes)
           murodes.physicsBody = SKPhysicsBody(rectangleOf: murosin.size)
           murodes.physicsBody?.affectedByGravity = false
           murodes.physicsBody?.isDynamic = false
           murodes.zPosition = 2
        btn.frame = CGRect(x: 20, y: 20, width: 100, height: 100)
        btn.setTitle("salta", for: .normal)
        btn.backgroundColor = .red
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        btn.addGestureRecognizer(tap)
        
        
          physicsWorld.contactDelegate = self
        self.physicsWorld.gravity = CGVector(dx:0.0 , dy:-6)
    
       var shape = SKSpriteNode()
        shape.position = CGPoint(x: -800, y: -200)
        shape.size = CGSize(width: 1500, height: 90)
        let text2 = SKTexture(image: UIImage(named: "pavimentoSin")!)
        shape = SKSpriteNode(texture: text2)
        shape.zPosition = 2
        addChild(shape)
       
        
        var shape2 = SKSpriteNode()
        shape2.size = CGSize(width: 400, height: 400)
        shape2.position = CGPoint(x: -400, y: 300)
        let text1 = SKTexture(image: UIImage(named: "pavimentoDes")!)
        shape2 = SKSpriteNode(texture: text1)
        shape2.zPosition = 2
        
        
        
        addChild(shape2)
   
        secondop.position = CGPoint(x: 1200, y: -200)
        
        let text3 = SKTexture(image: UIImage(named: "pavimentoDes")!)
        secondop = SKSpriteNode(texture: text3)
         secondop.zPosition = 2
        addChild(secondop)
        
        secondop.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 1098, height: 90))
        secondop.physicsBody?.affectedByGravity = false
        secondop.physicsBody!.isDynamic = false
        secondop.name="pavimento3"
        secondop.physicsBody!.categoryBitMask = orangeBallCategory
        secondop.physicsBody?.collisionBitMask = blueBallCategory
        secondop.physicsBody?.contactTestBitMask =  blueBallCategory
        
        
        
        terzop.position = CGPoint(x: 1200, y: -200)
        let text4 = SKTexture(image: UIImage(named: "pavimentoSin")!)
        terzop  = SKSpriteNode(texture: text3)
         terzop.zPosition = 2
        addChild(terzop)
        terzop.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 1500,height: 90))
        terzop.physicsBody?.affectedByGravity = false
        terzop.physicsBody!.isDynamic = false
        terzop.name="pavimento4"
        terzop.physicsBody!.categoryBitMask = orangeBallCategory
        terzop.physicsBody?.collisionBitMask = blueBallCategory
        terzop.physicsBody?.contactTestBitMask =  blueBallCategory
        
        
        shape2.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 700,height: 90))
        shape2.physicsBody?.affectedByGravity = false
        shape2.physicsBody!.isDynamic = false
        shape2.name="pavimento2"
        shape2.physicsBody!.categoryBitMask = pavimento2
        shape2.physicsBody?.collisionBitMask = blueBallCategory
        shape2.physicsBody?.contactTestBitMask =  blueBallCategory
      
        
        shape.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 1500, height: 90))
       
        
        shape.physicsBody?.affectedByGravity = false
        shape.physicsBody!.isDynamic = false
        shape.name="pavimento"
        shape.physicsBody!.categoryBitMask = orangeBallCategory
        shape.physicsBody?.collisionBitMask = blueBallCategory
        shape.physicsBody?.contactTestBitMask =  blueBallCategory
        
        
        shape1.position = CGPoint(x: 10, y: 10)
        let textpavimentosotto = SKTexture(image: UIImage(named: "pavimento")!)
        textpavimentosotto.filteringMode = SKTextureFilteringMode.nearest
        let textAscensorDes = SKTexture(image : UIImage(named: "ascensDes")!)
        ground2 = SKSpriteNode(texture: textAscensorDes)
        pavimentoSotto = SKSpriteNode(texture: textpavimentosotto)
        pavimentoSotto.name="pavimentosotto"
        pavimentoSotto.size = CGSize(width: 2400, height: 20)
        pavimentoSotto.position = CGPoint(x: 0, y: -525)
        addChild(pavimentoSotto)
        pavimentoSotto.physicsBody = SKPhysicsBody(rectangleOf: pavimentoSotto.frame.size)
        pavimentoSotto.physicsBody?.affectedByGravity = false
        pavimentoSotto.physicsBody?.isDynamic = false
        pavimentoSotto.physicsBody?.categoryBitMask = pavimentosott
        pavimentoSotto.physicsBody?.collisionBitMask = blueBallCategory
        pavimentoSotto.physicsBody?.contactTestBitMask = blueBallCategory
        pavimentoSotto.zPosition = 2
        
        
        
        
        let text = SKTexture(image: UIImage(named: "idle1")!)
        text.filteringMode = SKTextureFilteringMode.nearest
        shape1 = SKSpriteNode(texture: text)
        let nemtext = SKTexture(image: UIImage(named: "id1")!)
        nemtext.filteringMode = SKTextureFilteringMode.nearest
        let testext = SKTexture(image: UIImage(named: "taser")!)
         testext.filteringMode = SKTextureFilteringMode.nearest
        shape1.zPosition = 2
        nemico.zPosition=2
        taser = SKSpriteNode(texture: testext)
        taser.size = CGSize(width: 50, height: 50)
        taser.physicsBody = SKPhysicsBody(texture: testext, size: taser.frame.size) //modificare
        taser.physicsBody?.affectedByGravity = false
        taser.physicsBody?.isDynamic = false
        taser.physicsBody?.categoryBitMask = taser1
        taser.physicsBody?.collisionBitMask = nemic
        taser.physicsBody?.contactTestBitMask = nemic
        taser.name = "taser"
        taser.alpha = 0
       
        nemico = SKSpriteNode(texture: nemtext)
        nemico.size = CGSize(width: 200, height: 200)
        nemico.position = CGPoint(x: -100, y: 130)
        nemico.physicsBody?.mass = 200
        nemico.physicsBody = SKPhysicsBody(texture: nemtext, size:nemico.frame.size)
        nemico.physicsBody?.isDynamic = true
        nemico.physicsBody?.categoryBitMask = nemic
        nemico.physicsBody?.collisionBitMask = orangeBallCategory | pavimento2 | asc | blueBallCategory | pavimentose | pavimentoterz
        nemico.physicsBody?.contactTestBitMask =  orangeBallCategory | pavimento2 | asc | blueBallCategory | pavimentose | pavimentoterz
        nemico.physicsBody?.allowsRotation = false
        addChild(nemico)
        nemico.physicsBody?.restitution=0
        nemico.physicsBody?.friction = 1
        nemico.physicsBody?.mass = 1
        nemico.physicsBody?.applyForce(CGVector(dx: 0.0, dy: -20))
        shape1.size = CGSize(width: 300, height: 300)
        
       
        addChild(shape1)
        shape1.physicsBody?.applyForce(CGVector(dx: 0.0, dy: -20))
        shape1.physicsBody?.mass = 1
        shape1.physicsBody?.linearDamping = 0
         shape1.physicsBody?.angularDamping = 0
        nemico.physicsBody?.affectedByGravity = true
        nemico.name = "nemico"
        shape1.name="player"
        shape1.physicsBody = SKPhysicsBody(texture: text, size:shape1.frame.size)
        shape1.physicsBody?.restitution=0
        shape1.physicsBody?.affectedByGravity = true
       
        shape1.physicsBody?.friction = 1
     
       
//        shape1.physicsBody?.applyImpulse(CGVector(dx: -10.0, dy: 0.0))
       shape1.physicsBody?.collisionBitMask = orangeBallCategory | pavimento2 | asc | pavimentosott | pavimentose | pavimentoterz
         shape1.physicsBody?.categoryBitMask = blueBallCategory
        shape1.physicsBody?.contactTestBitMask =  orangeBallCategory | pavimento2 | asc | pavimentosott | pavimentose | pavimentoterz
        
        let textureAscensoreSin = SKTexture(image: UIImage(named: "ascensSin")!)
        ascens = SKSpriteNode(texture: textureAscensoreSin)
        ascens.zPosition = 2
        ascens.physicsBody = SKPhysicsBody(rectangleOf: ascens.frame.size)
        ascens.physicsBody?.isDynamic = false
        ascens.physicsBody?.affectedByGravity = false
        ascens.physicsBody?.collisionBitMask = blueBallCategory
        ascens.physicsBody?.categoryBitMask = asc
        ascens.physicsBody?.contactTestBitMask = asc
        addChild(ascens)
        ascens.position = CGPoint(x: -1000, y: 200)
        
        
        ground2.position = CGPoint(x:-700,y:-200)
        
        ground2.physicsBody = SKPhysicsBody(rectangleOf: ground2.frame.size)
        ground2.physicsBody?.restitution = 0
        ground2.physicsBody?.collisionBitMask = blueBallCategory
        ground2.physicsBody?.categoryBitMask = asc
        ground2.physicsBody?.contactTestBitMask = asc
        ground2.name = "ascensore"
        ground2.physicsBody?.restitution = 0
        ground2.physicsBody?.friction = 1
        self.addChild(ground2)
        
//        shape1.physicsBody?.collisionBitMask = pavimento2
//        shape1.physicsBody?.contactTestBitMask =  pavimento2
//        scene?.view?.showsPhysics = true
        
        let s1=SKTexture(image: UIImage(named: "idle1")!)
    
       
          changeTexture = SKAction.animate(with: [s1], timePerFrame: 0.4)
            
        shape1.run(SKAction.repeatForever(changeTexture))
        shape1.position = CGPoint(x: 1100, y: -350)
        shape.position = CGPoint(x: -800, y: -200)
        secondop.position = CGPoint(x: 700, y: -50)
        secondop.size = CGSize(width: 1098, height: 90)
        shape.size = CGSize(width: 1500, height: 90)
        shape2.position = CGPoint(x: 900, y: 220)
        shape2.size = CGSize(width: 700,height: 90)
        terzop.position = CGPoint(x: -800, y: 220)
        terzop.size = CGSize(width: 1500,height: 90)
        
        
        ascens.position = CGPoint(x: 500, y: 220)
        
        ground2.physicsBody?.affectedByGravity = false
        ground2.physicsBody?.isDynamic = false
        ground2.position = CGPoint(x:50,y:-200)
        ground2.zPosition=3
        shape1.physicsBody?.allowsRotation = false
        ascens.zPosition = 3
        nemico.zPosition = 2
      /*  nemico4 = SKSpriteNode(texture: nemtext)
             nemico4.position = CGPoint(x: -400, y:-350)
             addChild(nemico4)
             nemico4.physicsBody = SKPhysicsBody(texture: nemtext, size:nemico.frame.size)
             nemico4.physicsBody?.affectedByGravity = true
             nemico4.name = "nemico4"
             nemico4.physicsBody?.isDynamic = true
             nemico4.physicsBody?.collisionBitMask = orangeBallCategory |  pavimentosott | blueBallCategory
             nemico4.physicsBody?.contactTestBitMask =  orangeBallCategory |  pavimentosott | blueBallCategory
             nemico4.physicsBody?.allowsRotation = false
             nemico4.size = CGSize(width: 200, height: 200)
             nemico4.zPosition = 2*/
        nemico3 = SKSpriteNode(texture: nemtext)
        nemico3.position = CGPoint(x: 900, y:200)
        addChild(nemico3)
        nemico3.physicsBody = SKPhysicsBody(texture: nemtext, size:nemico.frame.size)
        nemico3.physicsBody?.affectedByGravity = true
        nemico3.name = "nemico3"
        nemico3.physicsBody?.isDynamic = true
        nemico3.physicsBody?.collisionBitMask = self.blueBallCategory|self.orangeBallCategory
        nemico3.physicsBody?.contactTestBitMask =  self.blueBallCategory|self.orangeBallCategory
        nemico3.physicsBody?.allowsRotation = false
        nemico3.size = CGSize(width: 200, height: 200)
        nemico3.zPosition = 2
        nemico2 = SKSpriteNode(texture: nemtext)
        nemico2.position = CGPoint(x: -100, y: 400)
        addChild(nemico2)
        nemico2.physicsBody = SKPhysicsBody(texture: nemtext, size:nemico.frame.size)
         nemico2.physicsBody?.affectedByGravity = true
        nemico2.name = "nemico2"
        nemico2.physicsBody?.isDynamic = true
        nemico2.physicsBody?.collisionBitMask = orangeBallCategory | pavimento2 | asc | blueBallCategory
        nemico2.physicsBody?.contactTestBitMask =  orangeBallCategory | pavimento2 | asc | blueBallCategory
        nemico2.physicsBody?.allowsRotation = false
        nemico2.size = CGSize(width: 200, height: 200)
        nemico2.zPosition = 2
        nemico3.physicsBody?.categoryBitMask = nemic
        
        


/*
var timer4 = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { timer in
    if(self.k == 0)
    {
        self.k=1
        var s=SKTexture(image: UIImage(named: "w1")!)
           var s1=SKTexture(image: UIImage(named: "w2")!)
           var s2=SKTexture(image: UIImage(named: "w3")!)
           var s3=SKTexture(image: UIImage(named: "w4")!)
        
        
        self.changeTexture = SKAction.animate(with: [s,s1,s2,s3], timePerFrame: 0.2)
        self.nemico4.run(SKAction.repeatForever(self.changeTexture))
    } else if (self.k == 1)
    {
       
        if(self.nemico4.position.x > -100)
        {
            print("ciao")
            self.k=2
        }
        else
        {
        print(self.shape1.position.x - self.nemico4.position.x)
        if(self.shape1.position.x - self.nemico4.position.x > -470 && self.shape1.position.x > self.nemico4.position.x && self.contatto == true)
                   {
                    if(self.mantienitext4 == 0)
                    {
                        let path = Bundle.main.path(forResource: "Ehi.wav", ofType:nil)!
                        let url = URL(fileURLWithPath: path)

                        do {
                          
                            self.PlayerEhi = try AVAudioPlayer(contentsOf: url)
                            self.PlayerEhi.play()
                          }
                         catch {
                           print("nada")
                        }
            
                        let s=SKTexture(image: UIImage(named: "run1")!)
                           let s1=SKTexture(image: UIImage(named: "run2")!)
                           let s2=SKTexture(image: UIImage(named: "run3")!)
                           let s3=SKTexture(image: UIImage(named: "run4")!)
                          let s4=SKTexture(image: UIImage(named: "run5")!)
                        let s5=SKTexture(image: UIImage(named: "run6")!)
                        let s6=SKTexture(image: UIImage(named: "run7")!)
                        self.changeTexture = SKAction.animate(with: [s,s1,s2,s3,s4,s5,s6], timePerFrame: 0.2)
                        self.nemico4.run(SKAction.repeatForever(self.changeTexture))
                        self.mantienitext4 = 1
                        print(self.mantienitext)
                        let path2 = Bundle.main.path(forResource: "VistaNemico.flac.flac", ofType:nil)!
                                                           let url2 = URL(fileURLWithPath: path2)

                                                           do {
                                                             
                                                               self.PlayerNemicoCorsa = try AVAudioPlayer(contentsOf: url2)
                                                               self.PlayerNemicoCorsa.play()
                                                             }
                                                            catch {
                                                              print("nada")
                                                           }
                    }
                    else{
                       self.nemico4.position = CGPoint(x: self.nemico4.position.x+10, y: self.nemico4.position.y)
                    }
                          
        }else {
                     self.nemico4.position = CGPoint(x: self.nemico4.position.x+5, y: self.nemico4.position.y)
            self.mantienitext4 = 0
        }
                   }
    }else if (self.k == 2)
    {
        self.nemico4.removeAllActions()
        let s = SKTexture(image: UIImage(named: "id1")!)
                           self.changeTexture = SKAction.animate(with: [s], timePerFrame: 0.2)
                           self.nemico4.run(self.changeTexture)
                           self.k=10
        let time = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: {
        timer in
          
            self.k = 3
        })
    }else if self.k==3{
       var s=SKTexture(image: UIImage(named: "w1s")!)
           var s1=SKTexture(image: UIImage(named: "w2s")!)
           var s2=SKTexture(image: UIImage(named: "w3s")!)
           var s3=SKTexture(image: UIImage(named: "w4s")!)
        
        self.k=4
        self.changeTexture = SKAction.animate(with: [s,s1,s2,s3], timePerFrame: 0.2)
        self.nemico4.run(SKAction.repeatForever(self.changeTexture))
        
    }else if (self.k==4)
    {
     
         if(self.nemico4.position.x < -600)
                       {
                           print("ciao")
                           self.k=5
                       }else {
            print(self.shape1.position.x - self.nemico4.position.x)
            if(self.shape1.position.x - self.nemico4.position.x < -270 && self.shape1.position.x < self.nemico4.position.x && self.contatto == true)
                       {
                        if(self.mantienitext4 == 0)
                        {
                            let path = Bundle.main.path(forResource: "Ehi.wav", ofType:nil)!
                                                 let url = URL(fileURLWithPath: path)

                                                 do {
                                                   
                                                     self.PlayerEhi = try AVAudioPlayer(contentsOf: url)
                                                     self.PlayerEhi.play()
                                                   }
                                                  catch {
                                                    print("nada")
                                                 }
                                       
                            self.mantienitext4 = 1
                            let s=SKTexture(image: UIImage(named: "run1s")!)
                               let s1=SKTexture(image: UIImage(named: "run2s")!)
                               let s2=SKTexture(image: UIImage(named: "run3s")!)
                               let s3=SKTexture(image: UIImage(named: "run4s")!)
                              let s4=SKTexture(image: UIImage(named: "run5s")!)
                            let s5=SKTexture(image: UIImage(named: "run6s")!)
                            let s6=SKTexture(image: UIImage(named: "run7s")!)
                            self.changeTexture = SKAction.animate(with: [s,s1,s2,s3,s4,s5,s6], timePerFrame: 0.2)
                            self.nemico4.run(SKAction.repeatForever(self.changeTexture))
                            let path2 = Bundle.main.path(forResource: "VistaNemico.flac.flac", ofType:nil)!
                                                               let url2 = URL(fileURLWithPath: path2)

                                                               do {
                                                                 
                                                                   self.PlayerNemicoCorsa = try AVAudioPlayer(contentsOf: url2)
                                                                   self.PlayerNemicoCorsa.play()
                                                                 }
                                                                catch {
                                                                  print("nada")
                                                               }
                        }
                        else{
                           self.nemico4.position = CGPoint(x: self.nemico4.position.x-20, y: self.nemico4.position.y)
                        }
                              
            }else {
                         self.nemico4.position = CGPoint(x: self.nemico4.position.x-10, y: self.nemico4.position.y)
                self.mantienitext4 = 0
            }
                       }
    }else if self.k==5{
        self.nemico4.removeAllActions()
                       let s = SKTexture(image: UIImage(named: "id1ss")!)
                                          self.changeTexture = SKAction.animate(with: [s], timePerFrame: 0.2)
                                          self.nemico4.run(self.changeTexture)
                                          self.k=10
                       let time = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: {
                       timer in
                         
                           self.k = 0
                       })
    }
 
    if(self.shape1.position.x <= self.nemico4.position.x && self.mortenemico4 == true)
    {
        print(self.k)
        self.salvaG = self.k
        self.k = 10
        let s = SKTexture(image: UIImage(named: "death1")!)
        let s1 = SKTexture(image: UIImage(named: "death2")!)
        let s2 = SKTexture(image: UIImage(named: "death3")!)
                           
        
                       var changet = SKAction.animate(with: [s,s1,s2], timePerFrame: 0.3)
                           let s3 = SKTexture(image: UIImage(named: "death4")!)
                          let s4 = SKTexture(image: UIImage(named: "death5")!)
                           let s5 = SKTexture(image: UIImage(named: "death6")!)
      
       self.nemico4.physicsBody?.collisionBitMask = self.pavimentosott

        self.nemico4.run(changet, completion: {
        changet = SKAction.animate(with: [s3,s4,s5], timePerFrame: 0.3)
        self.nemico4.run(SKAction.repeatForever(changet))
            let timer = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false, block: {
                timer in
                self.nemico4.removeAllActions()
                self.k = 3
                self.nemico4.physicsBody?.collisionBitMask = self.orangeBallCategory | self.pavimento2 | self.asc | self.blueBallCategory | self.pavimentosott
                self.stordito4 = 0
            })
                           })
    }
    if(self.shape1.position.x >= self.nemico4.position.x && self.mortenemico4 == true)
              {
                  print(self.k)
                  self.salvaG = self.k
                  
                  self.k = 10
                  let s = SKTexture(image: UIImage(named: "death1d")!)
                  let s1 = SKTexture(image: UIImage(named: "death2d")!)
                  let s2 = SKTexture(image: UIImage(named: "death3d")!)
                                 var changet = SKAction.animate(with: [s,s1,s2], timePerFrame: 0.3)
                                     let s3 = SKTexture(image: UIImage(named: "death4d")!)
                                    let s4 = SKTexture(image: UIImage(named: "death5d")!)
                                     let s5 = SKTexture(image: UIImage(named: "death6d")!)
                self.nemico4.physicsBody?.collisionBitMask = self.pavimentosott
                
                  self.nemico4.run(changet, completion: {
               changet = SKAction.animate(with: [s3,s4,s5], timePerFrame: 0.3)
                  self.nemico4.run(SKAction.repeatForever(changet))
                      let timer = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false, block: {
                          timer in
                          self.nemico4.removeAllActions()
                          self.k = 0
                        self.nemico4.physicsBody?.collisionBitMask = self.orangeBallCategory | self.pavimento2 | self.asc | self.pavimentosott | self.blueBallCategory
                        self.stordito4 = 0
                      })
                                     })
              }
    if (self.dead){
        if(self.vita > 0)
        {
            self.dead = false
            self.contatto4 = false
            self.shape1.position = CGPoint(x: 50, y: -50)
        }else{
            print("gameOver")
        }

    }


    
})*/


      

       var timer3 = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { timer in
           if(self.h == 0)
           {
               self.h=1
               var s=SKTexture(image: UIImage(named: "w1")!)
                  var s1=SKTexture(image: UIImage(named: "w2")!)
                  var s2=SKTexture(image: UIImage(named: "w3")!)
                  var s3=SKTexture(image: UIImage(named: "w4")!)
               
               
               self.changeTexture = SKAction.animate(with: [s,s1,s2,s3], timePerFrame: 0.2)
               self.nemico3.run(SKAction.repeatForever(self.changeTexture))
           } else if (self.h == 1)
           {
              
               if(self.nemico3.position.x > 1100)
               {
                   print("ciao")
                   self.h=2
               }
               else {
               print(self.shape1.position.x - self.nemico3.position.x)
               if(self.shape1.position.x - self.nemico3.position.x > -270 && self.shape1.position.x > self.nemico3.position.x && self.contatto3 == true)
                          {
                           if(self.mantienitext3 == 0)
                           {   let path = Bundle.main.path(forResource: "Ehi.wav", ofType:nil)!
                                                    let url = URL(fileURLWithPath: path)

                                                    do {
                                                      
                                                        self.PlayerEhi = try AVAudioPlayer(contentsOf: url)
                                                        self.PlayerEhi.play()
                                                      }
                                                     catch {
                                                       print("nada")
                                                    }
                                             
                               self.mantienitext3 = 1
                               let s=SKTexture(image: UIImage(named: "run1")!)
                                  let s1=SKTexture(image: UIImage(named: "run2")!)
                                  let s2=SKTexture(image: UIImage(named: "run3")!)
                                  let s3=SKTexture(image: UIImage(named: "run4")!)
                                 let s4=SKTexture(image: UIImage(named: "run5")!)
                               let s5=SKTexture(image: UIImage(named: "run6")!)
                               let s6=SKTexture(image: UIImage(named: "run7")!)
                               self.changeTexture = SKAction.animate(with: [s,s1,s2,s3,s4,s5,s6], timePerFrame: 0.2)
                               self.nemico3.run(SKAction.repeatForever(self.changeTexture))
                            let path2 = Bundle.main.path(forResource: "VistaNemico.flac", ofType:nil)!
                                                               let url2 = URL(fileURLWithPath: path2)

                                                               do {
                                                                 
                                                                   self.PlayerNemicoCorsa = try AVAudioPlayer(contentsOf: url2)
                                                                   self.PlayerNemicoCorsa.play()
                                                                 }
                                                                catch {
                                                                  print("nada")
                                                               }
                           }
                           else{
                              self.nemico3.position = CGPoint(x: self.nemico3.position.x+20, y: self.nemico3.position.y)
                           }
                                 
               }else {
                            self.nemico3.position = CGPoint(x: self.nemico3.position.x+10, y: self.nemico3.position.y)
                   self.mantienitext3 = 0
               }
                          }
           }else if (self.h == 2)
           {
               self.nemico3.removeAllActions()
               let s = SKTexture(image: UIImage(named: "id1")!)
                                  self.changeTexture = SKAction.animate(with: [s], timePerFrame: 0.2)
                                  self.nemico3.run(self.changeTexture)
                                  self.h=10
            self.timer10 = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: {
               timer in
                 
                   self.h = 3
               })
           }else if self.h==3{
              var s=SKTexture(image: UIImage(named: "w1s")!)
                  var s1=SKTexture(image: UIImage(named: "w2s")!)
                  var s2=SKTexture(image: UIImage(named: "w3s")!)
                  var s3=SKTexture(image: UIImage(named: "w4s")!)
               
               self.h=4
               self.changeTexture = SKAction.animate(with: [s,s1,s2,s3], timePerFrame: 0.2)
               self.nemico3.run(SKAction.repeatForever(self.changeTexture))
               
           }else if (self.h==4)
           {
            
                if(self.nemico3.position.x < 700)
                              {
                                  print("ciao")
                                  self.h=5
                              }else {
                   print(self.shape1.position.x - self.nemico3.position.x)
                   if(self.shape1.position.x - self.nemico3.position.x < 50 && self.shape1.position.x < self.nemico3.position.x && self.contatto3 == true)
                              {
                               if(self.mantienitext3 == 0)
                               {   let path = Bundle.main.path(forResource: "Ehi.wav", ofType:nil)!
                                                        let url = URL(fileURLWithPath: path)

                                                        do {
                                                          
                                                            self.PlayerEhi = try AVAudioPlayer(contentsOf: url)
                                                            self.PlayerEhi.play()
                                                          }
                                                         catch {
                                                           print("nada")
                                                        }
                                                       
                                   self.mantienitext3 = 1
                                   let s=SKTexture(image: UIImage(named: "run1s")!)
                                      let s1=SKTexture(image: UIImage(named: "run2s")!)
                                      let s2=SKTexture(image: UIImage(named: "run3s")!)
                                      let s3=SKTexture(image: UIImage(named: "run4s")!)
                                     let s4=SKTexture(image: UIImage(named: "run5s")!)
                                   let s5=SKTexture(image: UIImage(named: "run6s")!)
                                   let s6=SKTexture(image: UIImage(named: "run7s")!)
                                   self.changeTexture = SKAction.animate(with: [s,s1,s2,s3,s4,s5,s6], timePerFrame: 0.2)
                                   self.nemico3.run(SKAction.repeatForever(self.changeTexture))
                                let path2 = Bundle.main.path(forResource: "VistaNemico.flac", ofType:nil)!
                                                                   let url2 = URL(fileURLWithPath: path2)

                                                                   do {
                                                                     
                                                                       self.PlayerNemicoCorsa = try AVAudioPlayer(contentsOf: url2)
                                                                       self.PlayerNemicoCorsa.play()
                                                                     }
                                                                    catch {
                                                                      print("nada")
                                                                   }
                               }
                               else{
                                  self.nemico3.position = CGPoint(x: self.nemico3.position.x-20, y: self.nemico3.position.y)
                               }
                                     
                   }else {
                                self.nemico3.position = CGPoint(x: self.nemico3.position.x-10, y: self.nemico3.position.y)
                       self.mantienitext3 = 0
                   }
                              }
           }else if self.h==5{
               self.nemico3.removeAllActions()
                              let s = SKTexture(image: UIImage(named: "id1ss")!)
                                                 self.changeTexture = SKAction.animate(with: [s], timePerFrame: 0.2)
                                                 self.nemico3.run(self.changeTexture)
                                                 self.h=10
            self.timer20 = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: {
                              timer in
                                
                                  self.h = 0
                              })
           }
        
           if(self.shape1.position.x < self.nemico3.position.x && self.mortenemico3 == true)
           {
               print(self.h)
               self.salvaG = self.h
               self.mortenemico3 = false
               self.h = 10
               let s = SKTexture(image: UIImage(named: "death1")!)
               let s1 = SKTexture(image: UIImage(named: "death2")!)
               let s2 = SKTexture(image: UIImage(named: "death3")!)
            self.timer10.invalidate()
            self.timer20.invalidate()
            
                              var changet = SKAction.animate(with: [s,s1,s2], timePerFrame: 0.3)
                                  let s3 = SKTexture(image: UIImage(named: "death4")!)
                                 let s4 = SKTexture(image: UIImage(named: "death5")!)
                                  let s5 = SKTexture(image: UIImage(named: "death6")!)
                 self.nemico3.physicsBody?.collisionBitMask = self.orangeBallCategory
               self.nemico3.run(changet, completion: {
                                       changet = SKAction.animate(with: [s3,s4,s5], timePerFrame: 0.3)
               self.nemico3.run(SKAction.repeatForever(changet))
                   let timer = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false, block: {
                       timer in
                       self.nemico3.removeAllActions()
                       self.h = 3
                    self.nemico3.physicsBody?.collisionBitMask = self.orangeBallCategory | self.blueBallCategory
                    self.stordito3 = 0
                   })
                                  })
           }
           if(self.shape1.position.x > self.nemico3.position.x && self.mortenemico3 == true)
                     {
                         print(self.h)
                         self.salvaG = self.h
                         self.mortenemico3 = false
                         self.h = 10
                        self.timer10.invalidate()
                                 self.timer20.invalidate()
                         let s = SKTexture(image: UIImage(named: "death1d")!)
                         let s1 = SKTexture(image: UIImage(named: "death2d")!)
                         let s2 = SKTexture(image: UIImage(named: "death3d")!)
                        var changet = SKAction.animate(with: [s,s1,s2], timePerFrame: 0.3)
                          let s3 = SKTexture(image: UIImage(named: "death4d")!)
                         let s4 = SKTexture(image: UIImage(named: "death5d")!)
                         let s5 = SKTexture(image: UIImage(named: "death6d")!)
                          self.nemico3.physicsBody?.collisionBitMask = self.orangeBallCategory
                         self.nemico3.run(changet, completion: {
                                                 changet = SKAction.animate(with: [s3,s4,s5], timePerFrame: 0.3)
                         self.nemico3.run(SKAction.repeatForever(changet))
                             let timer = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false, block: {
                                 timer in
                                 self.nemico3.removeAllActions()
                                 self.h = 0
                                
                                self.nemico3.physicsBody?.collisionBitMask = self.orangeBallCategory | self.blueBallCategory
                                self.stordito3 = 0
                             })
                                            })
                     }
           if (self.dead){
               if(self.vita > 0)
               {
                   self.dead = false
                   self.contatto3 = false
                   self.shape1.position = CGPoint(x: 50, y: -50)
               }else{
                if(self.GameOver == false)
                                   {
                                    self.GameOver = true
                                   self.audioPlayer?.play()
                                                self.PlayerArtefatti.stop()
                                                self.PlayerTaser.stop()
                                               
                                                let scene = GameScene(fileNamed: "Menu")!
                                                scene.scaleMode = .aspectFill
                                                let transition = SKTransition.moveIn(with: .right, duration: 1)
                                                self.view?.presentScene(scene, transition: transition)
                                   print("gameOver")
                                   }
               }

           }


           
       })
       
        
        
          var timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { timer in
            if(self.g == 0)
            {
                self.g=1
                var s=SKTexture(image: UIImage(named: "w1")!)
                   var s1=SKTexture(image: UIImage(named: "w2")!)
                   var s2=SKTexture(image: UIImage(named: "w3")!)
                   var s3=SKTexture(image: UIImage(named: "w4")!)
                
                
                self.changeTexture = SKAction.animate(with: [s,s1,s2,s3], timePerFrame: 0.2)
                self.nemico2.run(SKAction.repeatForever(self.changeTexture))
            } else if (self.g == 1)
            {
               
                if(self.nemico2.position.x > -100)
                {
                    print("ciao")
                    self.g=2
                }
                else {
                  self.nemico2.position = CGPoint(x: self.nemico2.position.x+10, y: self.nemico2.position.y)
                }
            }else if (self.g == 2)
            {
                self.nemico2.removeAllActions()
                let s = SKTexture(image: UIImage(named: "id1")!)
                                   self.changeTexture = SKAction.animate(with: [s], timePerFrame: 0.2)
                                   self.nemico2.run(self.changeTexture)
                                   self.g=10
                self.timer30 = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: {
                timer in
                  
                    self.g = 3
                })
            }else if self.g==3{
               var s=SKTexture(image: UIImage(named: "w1s")!)
                   var s1=SKTexture(image: UIImage(named: "w2s")!)
                   var s2=SKTexture(image: UIImage(named: "w3s")!)
                   var s3=SKTexture(image: UIImage(named: "w4s")!)
                
                self.g=4
                self.changeTexture = SKAction.animate(with: [s,s1,s2,s3], timePerFrame: 0.2)
                self.nemico2.run(SKAction.repeatForever(self.changeTexture))
                
            }else if (self.g==4)
            {
                 if(self.nemico2.position.x < -500)
                               {
                                   print("ciao")
                                   self.g=5
                               }else {
                    print(self.shape1.position.x - self.nemico2.position.x)
                    if(self.shape1.position.x - self.nemico2.position.x > -270 && self.shape1.position.x < self.nemico2.position.x && self.contatto2 == true)
                               {
                                if(self.mantienitext2 == 0)
                                {   let path = Bundle.main.path(forResource: "Ehi.wav", ofType:nil)!
                                                         let url = URL(fileURLWithPath: path)

                                                         do {
                                                           
                                                             self.PlayerEhi = try AVAudioPlayer(contentsOf: url)
                                                             self.PlayerEhi.play()
                                                           }
                                                          catch {
                                                            print("nada")
                                                         }
                                                     
                                    self.mantienitext2 = 1
                                    let s=SKTexture(image: UIImage(named: "run1s")!)
                                       let s1=SKTexture(image: UIImage(named: "run2s")!)
                                       let s2=SKTexture(image: UIImage(named: "run3s")!)
                                       let s3=SKTexture(image: UIImage(named: "run4s")!)
                                      let s4=SKTexture(image: UIImage(named: "run5s")!)
                                    let s5=SKTexture(image: UIImage(named: "run6s")!)
                                    let s6=SKTexture(image: UIImage(named: "run7s")!)
                                    self.changeTexture = SKAction.animate(with: [s,s1,s2,s3,s4,s5,s6], timePerFrame: 0.2)
                                    self.nemico2.run(SKAction.repeatForever(self.changeTexture))
                                    let path2 = Bundle.main.path(forResource: "VistaNemico.flac", ofType:nil)!
                                                                       let url2 = URL(fileURLWithPath: path2)

                                                                       do {
                                                                         
                                                                           self.PlayerNemicoCorsa = try AVAudioPlayer(contentsOf: url2)
                                                                           self.PlayerNemicoCorsa.play()
                                                                         }
                                                                        catch {
                                                                          print("nada")
                                                                       }
                                }
                                else{
                                   self.nemico2.position = CGPoint(x: self.nemico2.position.x-20, y: self.nemico2.position.y)
                                }
                                      
                    }else {
                                 self.nemico2.position = CGPoint(x: self.nemico2.position.x-10, y: self.nemico2.position.y)
                        self.mantienitext2 = 0
                    }
                               }
            }else if self.g==5{
                self.nemico2.removeAllActions()
                               let s = SKTexture(image: UIImage(named: "id1ss")!)
                                                  self.changeTexture = SKAction.animate(with: [s], timePerFrame: 0.2)
                                                  self.nemico2.run(self.changeTexture)
                                                  self.g=10
                self.timer40  = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: {
                               timer in
                                 
                                   self.g = 0
                               })
            }
         
            if(self.shape1.position.x < self.nemico2.position.x && self.mortenemico2 == true)
            {
                print(self.g)
                self.salvaG = self.g
                self.mortenemico2 = false
                self.g = 10
                let s = SKTexture(image: UIImage(named: "death1")!)
                let s1 = SKTexture(image: UIImage(named: "death2")!)
                let s2 = SKTexture(image: UIImage(named: "death3")!)
                self.timer30.invalidate()
                self.timer40.invalidate()
                self.nemico2.physicsBody?.collisionBitMask = self.orangeBallCategory | self.pavimento2 | self.asc
                               var changet = SKAction.animate(with: [s,s1,s2], timePerFrame: 0.3)
                                   let s3 = SKTexture(image: UIImage(named: "death4")!)
                                  let s4 = SKTexture(image: UIImage(named: "death5")!)
                                   let s5 = SKTexture(image: UIImage(named: "death6")!)
               
                self.nemico2.run(changet, completion: {
                                        changet = SKAction.animate(with: [s3,s4,s5], timePerFrame: 0.3)
                self.nemico2.run(SKAction.repeatForever(changet))
                    let timer = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false, block: {
                        timer in
                        self.nemico2.removeAllActions()
                        self.g = 3
                        self.nemico2.physicsBody?.collisionBitMask = self.orangeBallCategory | self.pavimento2 | self.asc | self.blueBallCategory
                        self.stordito2 = 0
                    })
                                   })
            }
            if(self.shape1.position.x > self.nemico2.position.x && self.mortenemico2 == true)
                      {
                          print(self.g)
                          self.salvaG = self.g
                          self.mortenemico2 = false
                          self.g = 10
                        self.timer30.invalidate()
                                       self.timer40.invalidate()
                          let s = SKTexture(image: UIImage(named: "death1d")!)
                          let s1 = SKTexture(image: UIImage(named: "death2d")!)
                          let s2 = SKTexture(image: UIImage(named: "death3d")!)
                                             
                          self.nemico2.physicsBody?.collisionBitMask = self.orangeBallCategory | self.pavimento2 | self.asc
                                         var changet = SKAction.animate(with: [s,s1,s2], timePerFrame: 0.3)
                                             let s3 = SKTexture(image: UIImage(named: "death4d")!)
                                            let s4 = SKTexture(image: UIImage(named: "death5d")!)
                                             let s5 = SKTexture(image: UIImage(named: "death6d")!)
                          self.nemico2.physicsBody?.collisionBitMask = self.orangeBallCategory | self.pavimento2 | self.asc
                          self.nemico2.run(changet, completion: {
                                                  changet = SKAction.animate(with: [s3,s4,s5], timePerFrame: 0.3)
                          self.nemico2.run(SKAction.repeatForever(changet))
                              let timer = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false, block: {
                                  timer in
                                  self.nemico2.removeAllActions()
                                  self.g = 0
                                  self.nemico2.physicsBody?.collisionBitMask = self.orangeBallCategory | self.pavimento2 | self.asc | self.blueBallCategory
                                self.stordito2 = 0
                              })
                                             })
                      }
            if (self.dead){
                if(self.vita > 0)
                {
                    self.dead = false
                    self.contatto2 = false
                    self.shape1.position = CGPoint(x: 50, y: -50)
                }else{
                   if(self.GameOver == false)
                    {
                     self.GameOver = true
                    self.audioPlayer?.play()
                                 self.PlayerArtefatti.stop()
                                 self.PlayerTaser.stop()
                                
                                 let scene = GameScene(fileNamed: "Menu")!
                                 scene.scaleMode = .aspectFill
                                 let transition = SKTransition.moveIn(with: .right, duration: 1)
                                 self.view?.presentScene(scene, transition: transition)
                    print("gameOver")
                    }
                }

            }


            
        })
        
        if motionManager.isAccelerometerAvailable{
            motionManager.accelerometerUpdateInterval = 0.2
            motionManager.startAccelerometerUpdates(to: OperationQueue.current!){(data,error)in
                if let mydata = data
                {
                    if(mydata.acceleration.z > 0.1)
                    {
                        self.shake = 1
                        let path2 = Bundle.main.path(forResource: "elevator.wav", ofType:nil)!
                                                         let url2 = URL(fileURLWithPath: path2)

                                                         do {
                                                           
                                                             self.PlayerEnd = try AVAudioPlayer(contentsOf: url2)
                                                             self.PlayerEnd.play()
                                                           }
                                                          catch {
                                                            print("nada")
                                                         }
                    }
                    if(mydata.acceleration.z < -0.1 && self.shake == 1)
                    {
                        if self.ground2.position.y == -555 && self.ascens.position.y == 220
                        {self.ground2.removeAllActions()
                             self.ascens.removeAllActions()
                             let moverec = SKAction.moveTo(y: -200, duration: 1)
                            let moveasc = SKAction.moveTo(y: 220, duration: 1)
                             self.ascens.run(moveasc)
                            self.ground2.run(moverec)
                            
                            
                        
                        self.shake = 0
                        }else
                            if (self.ground2.position.y == -50 && self.ascens.position.y == -50){
                            
                            self.ground2.removeAllActions()
                             self.ascens.removeAllActions()
                             let moverec = SKAction.moveTo(y: -555, duration: 1.0)
                            self.ground2.run(moverec)
                             let moveasc = SKAction.moveTo(y: 220, duration: 1.0)
                             self.ascens.run(moveasc)
                                
                           
                           
                            self.shake = 0
                            
                            
                        }
                        else
                            {
                                if  self.ground2.position.y == 220 && self.ascens.position.y == 220
                                {
                                self.ground2.removeAllActions()
                                self.ascens.removeAllActions()
                                let moverec = SKAction.moveTo(y: -50, duration: 1.0)
                                self.ground2.run(moverec)
                                let moveasc = SKAction.moveTo(y: -50, duration: 1.0)
                                self.ascens.run(moveasc)
                                self.shake = 0
                                }
                                else
                                {
                                    self.ground2.removeAllActions()
                                    self.ascens.removeAllActions()
                                    let moverec = SKAction.moveTo(y: 220, duration: 1.0)
                                    self.ground2.run(moverec)
                                    let moveasc = SKAction.moveTo(y: 220, duration: 1.0)
                                    self.ascens.run(moveasc)
                                    self.shake = 0
                                }
                                
                        }
                    }
                }
            }
        }
       
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer?) {
        shape1.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 2.5))
//          shape1.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 0.05))
        btn.isUserInteractionEnabled = true
        btnpressed = true
        
        
    
    }
    func touchDown(atPoint pos : CGPoint) {
       
    }
    
    func touchMoved(toPoint pos : CGPoint) {
       
    }
    
    func touchUp(atPoint pos : CGPoint) {
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("entro")
        
        shape1.removeAllActions()
        
        b=0
      for touch: AnyObject in touches {
        var l = 0
        
        let location = touch.location(in: self)
        let node : SKNode = self.atPoint(location)
        if(artefatto1.alpha == 0 && artefatto2.alpha == 0 && artefatto3.alpha == 0 && artefatto4.alpha == 0  && mettiSire == false)
        {
           let test = SKTexture(image: UIImage(named: "SirenaGreen")!)
           sirena = SKSpriteNode(texture: test,size: CGSize(width: 70, height: 70) )
              
                  sirena.position = CGPoint(x: 1117, y: 524)
                  sirena.zPosition = 2
           portaFalsa.size = CGSize(width: 100, height: 100)
           portaFalsa.position = CGPoint(x: 1100, y: 410)
           portaFalsa.physicsBody = SKPhysicsBody(rectangleOf: portaFalsa.size)
           addChild(portaFalsa)
           portaFalsa.name = "porta"
           portaFalsa.physicsBody?.categoryBitMask = 8
           portaFalsa.physicsBody?.collisionBitMask = blueBallCategory
           portaFalsa.physicsBody?.contactTestBitMask = blueBallCategory
           portaFalsa.physicsBody?.isDynamic = false
           portaFalsa.physicsBody?.affectedByGravity = false
                  
                  sirena.zPosition = 2
                  addChild(sirena)
           
            
            mettiSire = true
        }
        
        
        
        if node.name == "art" {
            
            if(prendiArtEPianoSotto){
            let imgtex = SKTexture(image: UIImage(named: "Artefatto 6")!)
            let img = SKSpriteNode(texture: imgtex, size: imgtex.size())
            img.alpha = 0
            let path = Bundle.main.path(forResource: "Paper2.wav", ofType:nil)!
            let url = URL(fileURLWithPath: path)

            do {
              
                PlayerArtefatti = try AVAudioPlayer(contentsOf: url)
                PlayerArtefatti.play()
              }
             catch {
               print("nada")
            }
           addChild(img)
            let change = SKAction.fadeAlpha(to: 1.0, duration: 1.0)
            let change1 = SKAction.fadeAlpha(to: 0.0, duration: 1.0)
            img.run(change)
            img.zPosition = 4
            img.name = "artefatto1"
            node.run(change1)
        }
        }else if let prova = self.childNode(withName: "artefatto1") {
            let fadeout = SKAction.fadeAlpha(to: 0, duration: 1.0)
            prova.run(fadeout, completion: {prova.removeFromParent()})
            }
        
        if node.name == "art2" {
                   
            if(prendiArtPiano1){
                   let imgtex = SKTexture(image: UIImage(named: "artefatto 7")!)
                   let img = SKSpriteNode(texture: imgtex, size: imgtex.size())
                   img.alpha = 0
                             let path = Bundle.main.path(forResource: "Paper2.wav", ofType:nil)!
                             let url = URL(fileURLWithPath: path)

                             do {
                               
                                 PlayerArtefatti = try AVAudioPlayer(contentsOf: url)
                                 PlayerArtefatti.play()
                               }
                              catch {
                                print("nada")
                             }
                  addChild(img)
                   let change = SKAction.fadeAlpha(to: 1.0, duration: 1.0)
                   let change1 = SKAction.fadeAlpha(to: 0.0, duration: 1.0)
                   img.run(change)
                   img.zPosition = 4
                   img.name = "artefatto2"
                   node.run(change1)
               }
               }else if let prova = self.childNode(withName: "artefatto2") {
                   let fadeout = SKAction.fadeAlpha(to: 0, duration: 1.0)
                   prova.run(fadeout, completion: {prova.removeFromParent()})
                   }
        
        
        if node.name == "art3" {
            
            if(prendiArtPiano2){
            let imgtex = SKTexture(image: UIImage(named: "artefatto 8")!)
            let img = SKSpriteNode(texture: imgtex, size: imgtex.size())
            img.alpha = 0
                let path = Bundle.main.path(forResource: "Paper2.wav", ofType:nil)!
                let url = URL(fileURLWithPath: path)

                do {
                  
                    PlayerArtefatti = try AVAudioPlayer(contentsOf: url)
                    PlayerArtefatti.play()
                  }
                 catch {
                   print("nada")
                }
           addChild(img)
            let change = SKAction.fadeAlpha(to: 1.0, duration: 1.0)
            let change1 = SKAction.fadeAlpha(to: 0.0, duration: 1.0)
            img.run(change)
            img.zPosition = 4
            img.name = "artefatto3"
            node.run(change1)
        }
        }else if let prova = self.childNode(withName: "artefatto3") {
            let fadeout = SKAction.fadeAlpha(to: 0, duration: 1.0)
            prova.run(fadeout, completion: {prova.removeFromParent()})
            }
        
        
        
        
        
        if node.name == "art4" {
            
            if(prendiArtPiano3){
            let imgtex = SKTexture(image: UIImage(named: "artefatto 9")!)
            let img = SKSpriteNode(texture: imgtex, size: imgtex.size())
            img.alpha = 0
                let path = Bundle.main.path(forResource: "Paper2.wav", ofType:nil)!
                let url = URL(fileURLWithPath: path)

                do {
                  
                    PlayerArtefatti = try AVAudioPlayer(contentsOf: url)
                    PlayerArtefatti.play()
                  }
                 catch {
                   print("nada")
                }
           addChild(img)
            let change = SKAction.fadeAlpha(to: 1.0, duration: 1.0)
            let change1 = SKAction.fadeAlpha(to: 0.0, duration: 1.0)
            img.run(change)
            img.zPosition = 4
            img.name = "artefatto4"
            node.run(change1)
        }
        }else if let prova = self.childNode(withName: "artefatto4") {
            let fadeout = SKAction.fadeAlpha(to: 0, duration: 1.0)
            prova.run(fadeout, completion: {prova.removeFromParent()})
            }
        
        
        loc = location
        if (touch.tapCount >= 2)
       {
        let path = Bundle.main.path(forResource: "Teaser1.aiff", ofType:nil)!
        let url = URL(fileURLWithPath: path)

        do {
          
            PlayerTaser = try AVAudioPlayer(contentsOf: url)
            PlayerTaser.play()
          }
         catch {
           print("nada")
        }
        l = 1
        var location2 = touch.location(in: self)
             var loc2 = location
        taserfunz = true
          if(l==1)
          {
             
           //     self.addChild(self.soundteaser)
           
            l=0
        }
        if loc2.x < self.frame.midX{
            if(metti == 0){
                addChild(taser)
                metti = 1
            shape1.removeAllActions()
            let s=SKTexture(image: UIImage(named: "tasers")!)
            let s1=SKTexture(image: UIImage(named: "taser1s")!)
                let s3=SKTexture(image: UIImage(named: "idle1")!)
            b = 2
                changeTexture = SKAction.animate(with: [s,s1,s3], timePerFrame: 0.3)
            shape1.run(changeTexture)
            }
        }
        else
        {
            if(metti == 0){
            metti = 1
                addChild(taser)
            shape1.removeAllActions()
            let s=SKTexture(image: UIImage(named: "taserd")!)
                       let s1=SKTexture(image: UIImage(named: "taser1d")!)
                 let s2=SKTexture(image: UIImage(named: "idle1d")!)
                      b = 2
                       
            changeTexture = SKAction.animate(with: [s,s1,s2], timePerFrame: 0.2)
            shape1.run(changeTexture)
        }
        }
        let timer = Timer.scheduledTimer(withTimeInterval: 0.4, repeats: false, block: {timer in
            self.taserfunz = false
            self.metti = 0
            self.taser.removeFromParent()
            self.b = 0
            timer.invalidate()
            })

              
    
            
           
        
        }
    
          //Hold finger at upper area to move character constantly to the right.

      }
        if btnpressed == false {
          move = true
        }else{
            move = false
        }
      
        
        }

    
    
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    move = false
    print("fine")
    
    if(direzione == false)
    {
        if(b != 2)
       {
        let s1=SKTexture(image: UIImage(named: "idle1d")!)
        
        changeTexture = SKAction.animate(with: [s1], timePerFrame: 0.4)
      shape1.run(SKAction.repeatForever(changeTexture))
        } else{
            b=1
        }
    } else
    {
        if(b != 2)
        {
        let s1=SKTexture(image: UIImage(named: "idle1")!)
        
           changeTexture = SKAction.animate(with: [s1], timePerFrame: 0.4)
        shape1.run(SKAction.repeatForever(changeTexture))
        }else{
            b=1
        }
    }
    
    
        
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
       
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
      if(moveeni == false)
      {
        
        
        if(nemico.position.x-1 < -700)
        {
//            nemico.position = CGPoint(x: nemico.position.x, y: nemico.position.y)
           
            if(c==1)
                      {
                        self.mortenemico = false
                        self.dead = false
                        nemico.removeAllActions()
                       
                            c = 2
                            var s=SKTexture(image: UIImage(named: "id1ss")!)
                             
                             
                          
                          changeTexture = SKAction.animate(with: [s], timePerFrame: 0.5)
                        nemico.run(SKAction.repeatForever(changeTexture))
                        
                         timea = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false, block: { timer in
                            self.moveeni = true
                            self.c = 0
                          
                            timer.invalidate()
                                     })
                       
                        
                      }
            if(mortenemico == true){
                                   if(e == 0)
                                   {
                                    timea.invalidate()
                                   self.nemico.removeAllActions()
                                   e=1
                                   
                                   let s = SKTexture(image: UIImage(named: "death1")!)
                                   let s1 = SKTexture(image: UIImage(named: "death2")!)
                                   let s2 = SKTexture(image: UIImage(named: "death3")!)
                                       
                                   nemico.physicsBody?.collisionBitMask = orangeBallCategory | pavimento2 | asc
                                   var changet = SKAction.animate(with: [s,s1,s2], timePerFrame: 0.3)
                                       let s3 = SKTexture(image: UIImage(named: "death4")!)
                                      let s4 = SKTexture(image: UIImage(named: "death5")!)
                                       let s5 = SKTexture(image: UIImage(named: "death6")!)
                                       nemico.run(changet, completion: {
                                            changet = SKAction.animate(with: [s3,s4,s5], timePerFrame: 0.3)
                                           self.nemico.run(SKAction.repeatForever(changet))
                                       })
                                   
                                       let timer = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false, block: { timer in
                                        
                                           self.mortenemico = false
                                           self.e=0
                                           self.c = 0
                                          self.moveeni = true
                                        
                                           changet = SKAction.animate(with: [s3,s2,s1,s], timePerFrame: 0.3)
                                           self.nemico.run(changet)
                                           self.nemico.physicsBody?.collisionBitMask = self.orangeBallCategory | self.pavimento2 | self.asc | self.blueBallCategory
                                           self.dead = false
                                        self.stordito = 0
                                           
                                       })
                                   }
                                   
                               }
            else if (dead){
                         if(quellosopra == false){
                                                           if(vita > 0)
                                                           {
                                                            dead = false
                                                           shape1.position = CGPoint(x: 50, y: -450)
                                                           }else{
                                                            if(self.GameOver == false)
                                                            {
                                                             self.GameOver = true
                                                            self.audioPlayer?.play()
                                                                         self.PlayerArtefatti.stop()
                                                                         self.PlayerTaser.stop()
                                                                        
                                                                         let scene = GameScene(fileNamed: "Menu")!
                                                                         scene.scaleMode = .aspectFill
                                                                         let transition = SKTransition.moveIn(with: .right, duration: 1)
                                                                         self.view?.presentScene(scene, transition: transition)
                                                            print("gameOver")
                                                            }
                                                           }
                                                           }

            }
           
            
            
        }
        else
        {
            
            if(c==0 && e==0)
            {
                d=0
                nemico.removeAllActions()
                c=1
                var s=SKTexture(image: UIImage(named: "w1s")!)
                var s1=SKTexture(image: UIImage(named: "w2s")!)
                var s2=SKTexture(image: UIImage(named: "w3s")!)
                var s3=SKTexture(image: UIImage(named: "w4s")!)
                  
                
                changeTexture = SKAction.animate(with: [s,s1,s2,s3], timePerFrame: 0.2)
                nemico.run(SKAction.repeatForever(changeTexture))
               
                
                
            }
//            print(nemico.frame.origin.x - shape1.frame.origin.x)
            if(nemico.frame.origin.x - shape1.frame.origin.x  < 122 && contatto2 == true && mortenemico == false && shape1.frame.origin.x < nemico.frame.origin.x)
            {
                
                if(!dead && e==0){
                if(d == 0)
                {
                    d = 1
                    let s=SKTexture(image: UIImage(named: "run1s")!)
                       let s1=SKTexture(image: UIImage(named: "run2s")!)
                       let s2=SKTexture(image: UIImage(named: "run3s")!)
                       let s3=SKTexture(image: UIImage(named: "run4s")!)
                      let s4=SKTexture(image: UIImage(named: "run5s")!)
                    let s5=SKTexture(image: UIImage(named: "run6s")!)
                    let s6=SKTexture(image: UIImage(named: "run7s")!)
                    changeTexture = SKAction.animate(with: [s,s1,s2,s3,s4,s5,s6], timePerFrame: 0.2)
                    nemico.run(SKAction.repeatForever(changeTexture))
                    }
                    
                nemico.position = CGPoint(x: nemico.position.x-5, y: nemico.position.y)
                    
                }
                else{
                    
                    print(vita)
                    
           if(quellosopra == false){
                                                      if(vita > 0)
                                                      {
                                                       dead = false
                                                      shape1.position = CGPoint(x: 50, y: -450)
                                                      }else{
                                                       if(self.GameOver == false)
                                                        {
                                                         self.GameOver = true
                                                        self.audioPlayer?.play()
                                                                     self.PlayerArtefatti.stop()
                                                                     self.PlayerTaser.stop()
                                                                    
                                                                     let scene = GameScene(fileNamed: "Menu")!
                                                                     scene.scaleMode = .aspectFill
                                                                     let transition = SKTransition.moveIn(with: .right, duration: 1)
                                                                     self.view?.presentScene(scene, transition: transition)
                                                        print("gameOver")
                                                        }
                                                      }
                                                      }
                }
            }
            else if(mortenemico == true){
                if(e == 0)
                {
                self.nemico.removeAllActions()
                e=1
                
                let s = SKTexture(image: UIImage(named: "death1")!)
                let s1 = SKTexture(image: UIImage(named: "death2")!)
                let s2 = SKTexture(image: UIImage(named: "death3")!)
                    
                nemico.physicsBody?.collisionBitMask = orangeBallCategory | pavimento2 | asc
                var changet = SKAction.animate(with: [s,s1,s2], timePerFrame: 0.3)
                    let s3 = SKTexture(image: UIImage(named: "death4")!)
                   let s4 = SKTexture(image: UIImage(named: "death5")!)
                    let s5 = SKTexture(image: UIImage(named: "death6")!)
                    nemico.run(changet, completion: {
                         changet = SKAction.animate(with: [s3,s4,s5], timePerFrame: 0.3)
                        self.nemico.run(SKAction.repeatForever(changet))
                    })
                
                    let timer = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false, block: { timer in
                        self.mortenemico = false
                        self.e=0
                        self.c = 0
                       
                        changet = SKAction.animate(with: [s3,s2,s1,s], timePerFrame: 0.3)
                        self.nemico.run(changet)
                        self.nemico.physicsBody?.collisionBitMask = self.orangeBallCategory | self.pavimento2 | self.asc | self.blueBallCategory
                        self.dead = false
                        self.stordito = 0
                        
                    })
                }
                
            }else{
                if(e == 0)
                {
                  nemico.position = CGPoint(x: nemico.position.x-3, y: nemico.position.y)
                    
                }
            }
        }
      }else{
        if(nemico.position.x > -100
            )
                {
        //            nemico.position = CGPoint(x: nemico.position.x, y: nemico.position.y)
                    if(c==1)
                              {
                                d=0
                                nemico.removeAllActions()
                               c = 2
                                  var s=SKTexture(image: UIImage(named: "id1")!)
                                    
                                     
                                  
                                  changeTexture = SKAction.animate(with: [s], timePerFrame: 0.5)
                                nemico.run(SKAction.repeatForever(changeTexture))
                                s=SKTexture(image: UIImage(named: "id1ss")!)
                                    
                                     
                                  
                                  changeTexture = SKAction.animate(with: [s], timePerFrame: 0.5)
                                
                                  timea = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false, block: { timer in
                                    self.moveeni = false
                                    self.c = 0
                                    
                                                 timer.invalidate()
                                             })
                                timea1 = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false, block: { timer in
                                self.moveeni = false
                                self.c = 0
                                
                                             timer.invalidate()
                                         })
                              }
                   
                   else if(mortenemico == true){
                        if(e == 0)
                        {
                            
                            timea.invalidate()
                        self.nemico.removeAllActions()
                        e=1
                       
                        let s = SKTexture(image: UIImage(named: "death1d")!)
                        let s1 = SKTexture(image: UIImage(named: "death2d")!)
                        let s2 = SKTexture(image: UIImage(named: "death3d")!)
                            
                        nemico.physicsBody?.collisionBitMask = orangeBallCategory | pavimento2 | asc
                        var changet = SKAction.animate(with: [s,s1,s2], timePerFrame: 0.3)
                            let s3 = SKTexture(image: UIImage(named: "death4d")!)
                           let s4 = SKTexture(image: UIImage(named: "death5d")!)
                            let s5 = SKTexture(image: UIImage(named: "death6d")!)
                            nemico.run(changet, completion: {
                                 changet = SKAction.animate(with: [s3,s4,s5], timePerFrame: 0.3)
                                self.nemico.run(SKAction.repeatForever(changet))
                            })
                        
                            let timer = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false, block: { timer in
                                self.mortenemico = false
                                self.e=0
                                self.c = 0
                                self.moveeni = false
                                changet = SKAction.animate(with: [s3,s2,s1,s], timePerFrame: 0.3)
                                self.nemico.run(changet)
                                self.nemico.physicsBody?.collisionBitMask = self.orangeBallCategory | self.pavimento2 | self.asc | self.blueBallCategory
                                self.dead = false
                                self.stordito = 0
                                
                            })
                        }
                        
                    }else if (dead){
                              if(quellosopra == false){
                                                                   if(vita > 0)
                                                                   {
                                                                    dead = false
                                                                   shape1.position = CGPoint(x: 50, y: -450)
                                                                   }else{
                                                                   if(self.GameOver == false)
                                                                    {
                                                                     self.GameOver = true
                                                                    self.audioPlayer?.play()
                                                                                 self.PlayerArtefatti.stop()
                                                                                 self.PlayerTaser.stop()
                                                                                
                                                                                 let scene = GameScene(fileNamed: "Menu")!
                                                                                 scene.scaleMode = .aspectFill
                                                                                 let transition = SKTransition.moveIn(with: .right, duration: 1)
                                                                                 self.view?.presentScene(scene, transition: transition)
                                                                    print("gameOver")
                                                                    }
                                                                   }
                                                                   }

                    }
                    
                    
                   
                    
                    
        }else{
       
        if(c==0)
                   {
                    nemico.removeAllActions()
                       c=1
                       var s=SKTexture(image: UIImage(named: "w1")!)
                          var s1=SKTexture(image: UIImage(named: "w2")!)
                          var s2=SKTexture(image: UIImage(named: "w3")!)
                          var s3=SKTexture(image: UIImage(named: "w4")!)
                       
                       
                       changeTexture = SKAction.animate(with: [s,s1,s2,s3], timePerFrame: 0.2)
                       nemico.run(SKAction.repeatForever(changeTexture))
               
                   }
                  
            
       
            print(nemico.frame.origin.x - shape1.frame.origin.x)
           print (contatto)
            if(nemico.frame.origin.x - shape1.frame.origin.x  > -140 && contatto2 == true && mortenemico == false && shape1.frame.origin.x > nemico.frame.origin.x)
        {
            print (dead)
            print(e)
            if(!dead && e==0){
            if(d == 0)
            {   let path = Bundle.main.path(forResource: "Ehi.wav", ofType:nil)!
                                     let url = URL(fileURLWithPath: path)

                                     do {
                                       
                                         self.PlayerEhi = try AVAudioPlayer(contentsOf: url)
                                         self.PlayerEhi.play()
                                       }
                                      catch {
                                        print("nada")
                                     }
                                   
                
                d = 1
                let s=SKTexture(image: UIImage(named: "run1")!)
                   let s1=SKTexture(image: UIImage(named: "run2")!)
                   let s2=SKTexture(image: UIImage(named: "run3")!)
                   let s3=SKTexture(image: UIImage(named: "run4")!)
                  let s4=SKTexture(image: UIImage(named: "run5")!)
                let s5=SKTexture(image: UIImage(named: "run6")!)
                let s6=SKTexture(image: UIImage(named: "run7")!)
                changeTexture = SKAction.animate(with: [s,s1,s2,s3,s4,s5,s6], timePerFrame: 0.2)
                nemico.run(SKAction.repeatForever(changeTexture))
            let path2 = Bundle.main.path(forResource: "VistaNemico.flac", ofType:nil)!
                                               let url2 = URL(fileURLWithPath: path)

                                               do {
                                                 
                                                   self.PlayerNemicoCorsa = try AVAudioPlayer(contentsOf: url2)
                                                   self.PlayerNemicoCorsa.play()
                                                 }
                                                catch {
                                                  print("nada")
                                               }
                }
                
            nemico.position = CGPoint(x: nemico.position.x+5, y: nemico.position.y)
                
            }
            else if(e==0){
                
                print(vita)
                
                           if(quellosopra == false){
                                                           if(vita > 0)
                                                           {
                                                            dead = false
                                                           shape1.position = CGPoint(x: 50, y: -450)
                                                           }else{
                                                            if(self.GameOver == false)
                                                            {
                                                             self.GameOver = true
                                                            self.audioPlayer?.play()
                                                                         self.PlayerArtefatti.stop()
                                                                         self.PlayerTaser.stop()
                                                                        
                                                                         let scene = GameScene(fileNamed: "Menu")!
                                                                         scene.scaleMode = .aspectFill
                                                                         let transition = SKTransition.moveIn(with: .right, duration: 1)
                                                                         self.view?.presentScene(scene, transition: transition)
                                                            print("gameOver")
                                                            }
                                                           }
                                                           }
            }
            
        }else if(mortenemico == true){
            if(e == 0)
            {
            self.nemico.removeAllActions()
            e=1
            let s = SKTexture(image: UIImage(named: "death1d")!)
            let s1 = SKTexture(image: UIImage(named: "death2d")!)
            let s2 = SKTexture(image: UIImage(named: "death3d")!)
            
            nemico.physicsBody?.collisionBitMask = orangeBallCategory | pavimento2 | asc
            var changet = SKAction.animate(with: [s,s1,s2], timePerFrame: 0.3)
            nemico.run(changet)
            changet = SKAction.animate(with: [s,s1,s2], timePerFrame: 0.3)
                                           let s3 = SKTexture(image: UIImage(named: "death4d")!)
                                          let s4 = SKTexture(image: UIImage(named: "death5d")!)
                                           let s5 = SKTexture(image: UIImage(named: "death6d")!)
                                           nemico.run(changet, completion: {
                                                changet = SKAction.animate(with: [s3,s4,s5], timePerFrame: 0.3)
                                               self.nemico.run(SKAction.repeatForever(changet))
                                           })
                let timer = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false, block: { timer in
                    self.mortenemico = false
                    self.e=0
                    self.c = 0
                    changet = SKAction.animate(with: [s3,s2,s1,s], timePerFrame: 0.3)
                    self.nemico.run(changet)
                    self.nemico.physicsBody?.collisionBitMask = self.orangeBallCategory | self.pavimento2 | self.asc | self.blueBallCategory
                    self.dead = false
                    self.stordito = 0
                    
                })
        
        
        }
        }else{
        if(e == 0)
        {
          nemico.position = CGPoint(x: nemico.position.x+3, y: nemico.position.y)
            
        }
        }
        }
        }
        if (move == true)
        {
            if loc.y > 400{
                 
             

             }else{
            
               
               if loc.x < self.frame.midX{
               
                shape1.position = CGPoint(x: shape1.position.x-6, y: shape1.position.y)
                taser.position = CGPoint(x: shape1.position.x-110, y: shape1.frame.midY+20)
                
               direzione = true
               
                
                 if(loc.y > 100) {
                 
//                   shape1.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 0.50))
                    
                     print("vado1")
                   
                 }

               } else if loc.x > self.frame.midX{
               shape1.position = CGPoint(x: shape1.position.x+6, y: shape1.position.y)
                 taser.position = CGPoint(x: shape1.position.x+110, y: shape1.frame.midY+20)
                direzione = false
                 

              
                 if(loc.y > 100) {
//                    shape1.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 0.05))
                   
                 }
             }
        }
            if(b == 0)
                   {
                   b=1
                     shape1.removeAllActions()
                    if (direzione == false)
                    {
                   
                       let s=SKTexture(image: UIImage(named: "idle1d")!)
                       let s1=SKTexture(image: UIImage(named: "spd")!)
                       let s2=SKTexture(image: UIImage(named: "idle2d-1")!)
                    
                        changeTexture = SKAction.animate(with: [s,s1,s2], timePerFrame: 0.1)
                    }else
                    {
                        let s=SKTexture(image: UIImage(named: "idle1")!)
                        let s1=SKTexture(image: UIImage(named: "sps")!)
                        let s2=SKTexture(image: UIImage(named: "idle2d-2")!)
                        
                    
                         changeTexture = SKAction.animate(with: [s,s1,s2], timePerFrame: 0.1)
                    }
                        shape1.run(changeTexture, completion: {
                            
                            self.b=0
                            
                            })
                    
                       }
    }
        
       
            
           
        
    
               
}
    var audioPlayer : AVAudioPlayer? {
           get {
               let appDelegate = UIApplication.shared.delegate as! AppDelegate
               return appDelegate.audioPlayer
           }
           set {
               let appDelegate = UIApplication.shared.delegate as! AppDelegate
               appDelegate.audioPlayer = newValue
           }
       }
    
    func didBegin(_ contact: SKPhysicsContact) {
        print(contact.bodyA.node!.name)
        print(contact.bodyB.node!.name)
        if(contact.bodyA.node!.name == "pavimentosotto" && contact.bodyB.node!.name=="player" )
        {
         contatto = true
            prendiArtEPianoSotto = true
            prendiArtPiano1 = false
            prendiArtPiano2 = false
            prendiArtPiano3 = false
            contatto2 = false
            contatto3 = false
            contatto4 = false
            
        }
        if(contact.bodyA.node!.name == "pavimento" && contact.bodyB.node!.name=="player")
        {
            contatto2 = true
            prendiArtPiano1 = true
            prendiArtEPianoSotto = false
                      prendiArtPiano2 = false
                      prendiArtPiano3 = false
            contatto3 = false
                       contatto4 = false
            contatto = false
        }
        if(contact.bodyA.node!.name == "pavimento3" && contact.bodyB.node!.name=="player")
        {
            contatto3 = true
            prendiArtPiano2 = true
            prendiArtEPianoSotto = false
                      prendiArtPiano1 = false
                      prendiArtPiano3 = false
            contatto2 = false
                       contatto4 = false
            contatto = false
        }
        if(contact.bodyA.node!.name == "pavimento4" && contact.bodyB.node!.name=="player")
        {
            contatto4 = true
            
            prendiArtEPianoSotto = false
                      prendiArtPiano1 = false
                      prendiArtPiano2 = false
                      prendiArtPiano3 = true
            contatto2 = false
            contatto3 = false
            contatto = false
        }
        
        if contact.bodyA.node!.name == "nemico" && contact.bodyB.node!.name=="player" && self.taserfunz==false && stordito == 0
        {
            if (e == 0)
                      {
                      if(ancoraflag == 0)
                      {
                        let cgange = SKAction.fadeAlpha(to: 1.0, duration: 1.0)
                                           artefatto1.run(cgange)
                                           artefatto2.run(cgange)
                                           artefatto3.run(cgange)
                                           artefatto4.run(cgange)
                                          portaFalsa.removeFromParent()
                                           sirena.removeFromParent()
                                           mettiSire = false
                      dead = true
                     
                        vita-=1
                                                  shape1.position = CGPoint(x: 10, y: -350)
                                               if(vita == 2)
                                               {
                                                   cuore3.texture = SKTexture(image: UIImage(named: "CuoreVuoto")!)
                                               }else if vita == 1{
                                                   cuore2.texture = SKTexture(image: UIImage(named: "CuoreVuoto")!)
                                               } else{
                                                   cuore1.texture = SKTexture(image: UIImage(named: "CuoreVuoto")!)
                                                   }
                      mortenemico = false
                      ancoraflag+=1
                        
                      }else{
                      ancoraflag = 0
                      }
                                }
        }
    
        else if(contact.bodyA.node!.name == "nemico" && contact.bodyB.node!.name=="taser" && taserfunz == true)
        {
            dead = false
            mortenemico = true
            stordito = 1
        }
        
        if(contact.bodyB.node!.name == "nemico2" && contact.bodyA.node!.name=="player" && taserfunz == false && stordito2 == 0)
              {
                if(ancoraflag == 0)
                  {
                    let cgange = SKAction.fadeAlpha(to: 1.0, duration: 1.0)
                                       artefatto1.run(cgange)
                                       artefatto2.run(cgange)
                                       artefatto3.run(cgange)
                                       artefatto4.run(cgange)
                            portaFalsa.removeFromParent()
                                       sirena.removeFromParent()
                                       mettiSire = false
                  dead = true
                  vita-=1
                                            shape1.position = CGPoint(x: 10, y: -350)
                                         if(vita == 2)
                                         {
                                             cuore3.texture = SKTexture(image: UIImage(named: "CuoreVuoto")!)
                                         }else if vita == 1{
                                             cuore2.texture = SKTexture(image: UIImage(named: "CuoreVuoto")!)
                                         } else{
                                             cuore1.texture = SKTexture(image: UIImage(named: "CuoreVuoto")!)
                                             }
                  mortenemico2 = false
                  ancoraflag+=1
                  let timer = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: {
                                   timer in
                                   self.ancoraflag = 0
                                   self.quellosopra = false
                               })
                  }else{
                  ancoraflag = 0
                  }
                }
              
              else if(contact.bodyA.node!.name == "nemico2" && contact.bodyB.node!.name=="taser" && taserfunz == true)
              {
                  dead = false
                  mortenemico2 = true
                  stordito2 = 1
              }
            if(contact.bodyB.node!.name=="player" && contact.bodyA.node!.name == "nemico3" && taserfunz == false && stordito == 1)
            {
              if(ancoraflag == 0)
                {
                    let cgange = SKAction.fadeAlpha(to: 1.0, duration: 1.0)
                                       artefatto1.run(cgange)
                                       artefatto2.run(cgange)
                                       artefatto3.run(cgange)
                                       artefatto4.run(cgange)
                    portaFalsa.removeFromParent()
                                       sirena.removeFromParent()
                                       mettiSire = false

                    
                dead = true
                 vita-=1
                                         shape1.position = CGPoint(x: 10, y: -350)
                                       if(vita == 2)
                                       {
                                           cuore3.texture = SKTexture(image: UIImage(named: "CuoreVuoto")!)
                                       }else if vita == 1{
                                           cuore2.texture = SKTexture(image: UIImage(named: "CuoreVuoto")!)
                                       } else{
                                           cuore1.texture = SKTexture(image: UIImage(named: "CuoreVuoto")!)
                                           }
                mortenemico3 = false
                ancoraflag+=1
                let timer = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: {
                                 timer in
                                 self.ancoraflag = 0
                                 self.quellosopra = false
                             })
                }else{
                ancoraflag = 0
                }
              }
            
            else if(contact.bodyA.node!.name == "nemico3" && contact.bodyB.node!.name=="taser" && taserfunz == true)
            {
                dead = false
                mortenemico3 = true
                stordito3 = 1
            }
        if(contact.bodyB.node!.name == "nemico4" && contact.bodyA.node!.name=="player" && taserfunz == false && stordito4==0)
        {
           
           
          if(ancoraflag == 0)
            {
                
            dead = true
             vita-=1
                                      shape1.position = CGPoint(x: 10, y: -350)
                                   if(vita == 2)
                                   {
                                       cuore3.texture = SKTexture(image: UIImage(named: "CuoreVuoto")!)
                                   }else if vita == 1{
                                       cuore2.texture = SKTexture(image: UIImage(named: "CuoreVuoto")!)
                                   } else{
                                       cuore1.texture = SKTexture(image: UIImage(named: "CuoreVuoto")!)
                                       }
            mortenemico4 = false
            ancoraflag+=1
            let timer = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: {
                             timer in
                             self.ancoraflag = 0
                             self.quellosopra = false
                         })
            }else{
            ancoraflag = 0
            }
          }
        
        else if(contact.bodyA.node!.name == "nemico4" && contact.bodyB.node!.name=="taser" && taserfunz == true)
        {
            
            dead = false
            mortenemico4 = true
            stordito4=1
        }
        
        if (contact.bodyA.node!.name == "ascensore" && contact.bodyB.node!.name=="player" )
        {
            
        }
        
        if (contact.bodyA.node!.name == "player" && contact.bodyB.node!.name=="porta" )
        {
            audioPlayer?.play()
            PlayerTaser.stop()
            PlayerEnd.stop()
            PlayerEhi.stop()
            PlayerArtefatti.stop()
            let scene = GameScene(fileNamed: "Finale")!
                                             scene.scaleMode = .aspectFill
                                             let transition = SKTransition.moveIn(with: .right, duration: 1)
                                             self.view?.presentScene(scene, transition: transition)
        }

    }
  
}

