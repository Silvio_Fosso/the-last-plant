//
//  MainMenu.swift
//  Schermata_iniziale
//
//  Created by Dario Mandarino on 22/11/2019.
//  Copyright © 2019 Dario Mandarino. All rights reserved.
//

import Foundation
import SpriteKit

class MainMenu: SKScene  {
    
    /* UI Connections */
    var buttonPlay: MSButtonNode!
    var buttonCredits: MSButtonNode!
    
        override func didMove(to view: SKView) {
            /* Setup your scene here */

            /* Set UI connections */
            buttonPlay = self.childNode(withName: "buttonPlay") as! MSButtonNode
                  buttonPlay.selectedHandler = {
                self.loadGame()
            }
            buttonCredits = self.childNode(withName: "buttonCredits") as! MSButtonNode
            buttonCredits.selectedHandler = {
                self.loadCredits()
            }
        }
    
    func loadGame() {
        /* 1) Grab reference to our SpriteKit view */
        guard let skView = self.view as SKView? else {
            print("Could not get Skview")
            return
        }

        /* 2) Load Game scene */
        guard let scene = GameScene(fileNamed:"GameScene") else {
            print("Could not make GameScene, check the name is spelled correctly")
            return
        }

        /* 3) Ensure correct aspect mode */
        scene.scaleMode = .aspectFill

        /* Show debug */
        skView.showsPhysics = true
        skView.showsDrawCount = true
        skView.showsFPS = true

        /* 4) Start game scene */
        skView.presentScene(scene)
    }
    
    func loadCredits() {
        /* 1) Grab reference to our SpriteKit view */
        guard let skView = self.view as SKView? else {
            print("Could not get Skview")
            return
        }

        /* 2) Load Game scene */
        guard let scene = GameScene(fileNamed:"CreditScene") else {
            print("Could not make GameScene, check the name is spelled correctly")
            return
        }

        /* 3) Ensure correct aspect mode */
        scene.scaleMode = .aspectFill

        /* Show debug */
        skView.showsPhysics = true
        skView.showsDrawCount = true
        skView.showsFPS = true

        /* 4) Start game scene */
        skView.presentScene(scene)
    }
}
