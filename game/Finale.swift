//
//  Finale.swift
//  game
//
//  Created by Silvio Fosso on 25/11/2019.
//  Copyright © 2019 Silvio Fosso. All rights reserved.
//

import Foundation
import SpriteKit
class Finale: SKScene,SKPhysicsContactDelegate {
    override func didMove(to view: SKView) {
      let text = SKTexture(image: UIImage(named: "Schermata")!)
       let Schermata = SKSpriteNode(texture: text,size: text.size())
        Schermata.alpha = 0
         
        Schermata.position = CGPoint(x: 0, y: 0)
        addChild(Schermata)
        let change = SKAction.fadeAlpha(to: 1.0, duration: 1.0)
        Schermata.run(change)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
                                    let scene = GameScene(fileNamed: "Credits")!
                                      scene.scaleMode = .aspectFill
                                      let transition = SKTransition.moveIn(with: .right, duration: 1)
                                      self.view?.presentScene(scene, transition: transition)
    }
}
