//
//  GameScene.swift
//  game
//
//  Created by Silvio Fosso on 07/11/2019.
//  Copyright © 2019 Silvio Fosso. All rights reserved.
//

import SpriteKit
import GameplayKit
import CoreMotion
import AVFoundation
class GameScene: SKScene,SKPhysicsContactDelegate {
    var motionManager = CMMotionManager()
    var shake = 0
    var taser = SKSpriteNode()
    let blueBallCategory : UInt32 = 0x1 << 0
    let orangeBallCategory : UInt32 = 0x1 << 1
    let pavimento2 : UInt32 = 0x1 << 2
    let nemic : UInt32 = 0x1 << 4
    let asc : UInt32 = 0x1 << 3
    let taser1 : UInt32 = 0x1 << 5
    let pavimentosott : UInt32 = 0x1 << 6
    var move = false
    var shape1 = SKSpriteNode()
    var a = 0
    var b=0
    var c = 0
    var d = 0
    var stordito2 = 0
    var mortenemico = false
    var nemico = SKSpriteNode()
    var loc = CGPoint()
    var btn = UIButton()
    var ground2 = SKSpriteNode()
    var direzione : Bool = false
    var btnpressed : Bool = false
    var changeTexture = SKAction()
    var moveeni = false
    var contatto = false
    var dead = false
    var taserfunz = false
    var ascens = SKSpriteNode()
    var e = 0
    var vita = 3
    var ancoraflag = 0
    var metti = 0
    var timea = Timer()
    var timea1 = Timer()
    var pavimentoSotto = SKSpriteNode()
    var nemico2 = SKSpriteNode()
    var contatto2 = false
    var mortenemico2 = false
    var g = 0
    var m = 0
    var sirena = SKSpriteNode()
    var mantienitext = 0
    var salvaG = 0
    var stordito = 0
    var stordito3 = 0
    var quellosopra = false
    var cuore1 = SKSpriteNode()
   var timer1 = Timer()
    var timer2 = Timer()
   var timer3 = Timer()  // timer 1
    var timer4 = Timer() // timer 2
    var cuore2 = SKSpriteNode()
    var fulmine = SKSpriteNode()
    var cuore3 = SKSpriteNode()
    var artefatto1 = SKSpriteNode()
    var artefatto2 = SKSpriteNode()
    var artefatto3 = SKSpriteNode()
    var artefatto4 = SKSpriteNode()
    var artefatto5 = SKSpriteNode()
    var prendiArtEPianoSotto = false
    var prendiArtPiano1 = false
    var prendiArtPiano2 = false
    var mantienitext2 = 0
    var mortenemico3 = false
    var nemico3 = SKSpriteNode()
    var contatto3 = false
    var mettiSire = false
    var Background = AVAudioPlayer()
    var PlayerArtefatti = AVAudioPlayer()
    var PlayerTaser = AVAudioPlayer()
    var NemicoEhi = AVAudioPlayer()
    var NemicoCorsa = AVAudioPlayer()
    var Elevator = AVAudioPlayer()
    var portaFalsa = SKSpriteNode()
    var GameOver = false
    override func didMove(to view: SKView) {
        btn.frame = CGRect(x: 20, y: 20, width: 100, height: 100)
        btn.setTitle("salta", for: .normal)
        btn.backgroundColor = .red
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        btn.addGestureRecognizer(tap)
        audioPlayer?.pause()
      
        
    
          physicsWorld.contactDelegate = self
        self.physicsWorld.gravity = CGVector(dx:0.0 , dy:-6)
    
       var shape = SKSpriteNode()
        shape.size = CGSize(width: 350, height: 400)
        shape.position = CGPoint(x: -350, y: 0)
        let text2 = SKTexture(image: UIImage(named: "pavimentoSin")!)
        shape = SKSpriteNode(texture: text2)
         shape.zPosition = 2
        addChild(shape)
       
        
        var shape2 = SKSpriteNode()
        shape2.size = CGSize(width: 400, height: 400)
        shape2.position = CGPoint(x: -350, y: 300)
        let text1 = SKTexture(image: UIImage(named: "pavimentoDes")!)
        shape2 = SKSpriteNode(texture: text1)
        shape2.zPosition = 2
        
        addChild(shape2)
        shape2.physicsBody = SKPhysicsBody(rectangleOf: shape2.frame.size)
        shape2.physicsBody?.affectedByGravity = false
        shape2.physicsBody!.isDynamic = false
        shape2.name="pavimento2"
        shape2.physicsBody!.categoryBitMask = pavimento2
        shape2.physicsBody?.collisionBitMask = blueBallCategory
        shape2.physicsBody?.contactTestBitMask =  blueBallCategory
      
        
        shape.physicsBody = SKPhysicsBody(edgeLoopFrom: shape.frame)
       
        
        shape.physicsBody?.affectedByGravity = false
        shape.physicsBody!.isDynamic = false
        shape.name="pavimento"
        shape.physicsBody!.categoryBitMask = orangeBallCategory
        shape.physicsBody?.collisionBitMask = blueBallCategory
        shape.physicsBody?.contactTestBitMask =  blueBallCategory
        
        
        shape1.position = CGPoint(x: 10, y: 10)
        let textpavimentosotto = SKTexture(image: UIImage(named: "pavimento")!)
        textpavimentosotto.filteringMode = SKTextureFilteringMode.nearest
        let textAscensorDes = SKTexture(image : UIImage(named: "ascensDes")!)
        ground2 = SKSpriteNode(texture: textAscensorDes)
        pavimentoSotto = SKSpriteNode(texture: textpavimentosotto)
        pavimentoSotto.name="pavimentosotto"
        pavimentoSotto.size = CGSize(width: 2400, height: 20)
        pavimentoSotto.position = CGPoint(x: 0, y: -525)
        addChild(pavimentoSotto)
        pavimentoSotto.physicsBody = SKPhysicsBody(rectangleOf: pavimentoSotto.frame.size)
        pavimentoSotto.physicsBody?.affectedByGravity = false
        pavimentoSotto.physicsBody?.isDynamic = false
        pavimentoSotto.physicsBody?.categoryBitMask = pavimentosott
        pavimentoSotto.physicsBody?.collisionBitMask = blueBallCategory | nemic
        pavimentoSotto.physicsBody?.contactTestBitMask = blueBallCategory | nemic
        pavimentoSotto.zPosition = 2
        
        
        
        let text = SKTexture(image: UIImage(named: "idle1")!)
        text.filteringMode = SKTextureFilteringMode.nearest
        shape1 = SKSpriteNode(texture: text)
        let nemtext = SKTexture(image: UIImage(named: "id1")!)
        nemtext.filteringMode = SKTextureFilteringMode.nearest
        let testext = SKTexture(image: UIImage(named: "taser")!)
         testext.filteringMode = SKTextureFilteringMode.nearest
        shape1.zPosition = 2
        nemico.zPosition=2
        taser = SKSpriteNode(texture: testext)
        taser.size = CGSize(width: 50, height: 50)
        taser.physicsBody = SKPhysicsBody(texture: testext, size: taser.frame.size)
        taser.physicsBody?.affectedByGravity = false
        taser.physicsBody?.isDynamic = false
        taser.physicsBody?.categoryBitMask = taser1
        taser.physicsBody?.collisionBitMask = nemic
        taser.physicsBody?.contactTestBitMask = nemic
        taser.name = "taser"
        taser.alpha = 0
       
        nemico = SKSpriteNode(texture: nemtext)
        nemico.size = CGSize(width: 200, height: 200)
        nemico.position = CGPoint(x: 50, y: 0)
        nemico.physicsBody?.mass = 200
        nemico.physicsBody = SKPhysicsBody(texture: nemtext, size:nemico.frame.size)
        nemico.physicsBody?.isDynamic = true
        nemico.physicsBody?.categoryBitMask = nemic
        
        nemico.physicsBody?.collisionBitMask = orangeBallCategory | pavimento2 | asc | blueBallCategory
        nemico.physicsBody?.contactTestBitMask =  orangeBallCategory | pavimento2 | asc | blueBallCategory
        nemico.physicsBody?.allowsRotation = false
        addChild(nemico)
        nemico.physicsBody?.restitution=0
        nemico.physicsBody?.friction = 1
        nemico.physicsBody?.mass = 1
        nemico.physicsBody?.applyForce(CGVector(dx: 0.0, dy: -20))
        shape1.size = CGSize(width: 300, height: 300)
        
       
        addChild(shape1)
        shape1.physicsBody?.applyForce(CGVector(dx: 0.0, dy: -20))
        shape1.physicsBody?.mass = 1
        shape1.physicsBody?.linearDamping = 0
         shape1.physicsBody?.angularDamping = 0
        nemico.physicsBody?.affectedByGravity = true
        nemico.name = "nemico"
        shape1.name="player"
        shape1.physicsBody = SKPhysicsBody(texture: text, size:shape1.frame.size)
        shape1.physicsBody?.restitution=0
        shape1.physicsBody?.affectedByGravity = true
       
        shape1.physicsBody?.friction = 1
     
       
//        shape1.physicsBody?.applyImpulse(CGVector(dx: -10.0, dy: 0.0))
       shape1.physicsBody?.collisionBitMask = orangeBallCategory | pavimento2 | asc | pavimentosott
         shape1.physicsBody?.categoryBitMask = blueBallCategory
        shape1.physicsBody?.contactTestBitMask =  orangeBallCategory | pavimento2 | asc | pavimentosott
        
        let textureAscensoreSin = SKTexture(image: UIImage(named: "ascensSin")!)
        ascens = SKSpriteNode(texture: textureAscensoreSin)
        ascens.zPosition = 2
        ascens.physicsBody = SKPhysicsBody(rectangleOf: ascens.frame.size)
        ascens.physicsBody?.isDynamic = false
        ascens.physicsBody?.affectedByGravity = false
        ascens.physicsBody?.collisionBitMask = blueBallCategory
        ascens.physicsBody?.categoryBitMask = asc
        ascens.physicsBody?.contactTestBitMask = asc
        addChild(ascens)
        ascens.position = CGPoint(x: -1000, y: 200)
        
        
        ground2.position = CGPoint(x:-290,y:-94)
        
        ground2.physicsBody = SKPhysicsBody(rectangleOf: ground2.frame.size)
        ground2.physicsBody?.restitution = 0
        ground2.physicsBody?.collisionBitMask = blueBallCategory
        ground2.physicsBody?.categoryBitMask = asc
        ground2.physicsBody?.contactTestBitMask = asc
        ground2.name = "ascensore"
        ground2.physicsBody?.restitution = 0
        ground2.physicsBody?.friction = 1
        self.addChild(ground2)
        
//        shape1.physicsBody?.collisionBitMask = pavimento2
//        shape1.physicsBody?.contactTestBitMask =  pavimento2
//      scene?.view?.showsPhysics = true
        
        let s1=SKTexture(image: UIImage(named: "idle1")!)
        
       
          changeTexture = SKAction.animate(with: [s1], timePerFrame: 0.4)
            
         shape1.run(SKAction.repeatForever(changeTexture))
        shape1.position = CGPoint(x: -700, y: -280)
        shape.position = CGPoint(x: -100, y: -130)
         shape.size = CGSize(width: 2160, height: 90)
         shape2.position = CGPoint(x: 170, y: 200)
        ground2.physicsBody?.affectedByGravity = false
              ground2.physicsBody?.isDynamic = false
        ground2.position = CGPoint(x: 1080, y: -130)
        ground2.zPosition=3
        shape1.physicsBody?.allowsRotation = false
        ascens.zPosition = 3
        nemico.zPosition = 2
        nemico2 = SKSpriteNode(texture: nemtext)
        nemico2.position = CGPoint(x: 70, y: 300)
        addChild(nemico2)
        nemico2.physicsBody = SKPhysicsBody(texture: nemtext, size:nemico.frame.size)
         nemico2.physicsBody?.affectedByGravity = true
        nemico2.name = "nemico2"
        nemico2.physicsBody?.isDynamic = true
        nemico2.physicsBody?.collisionBitMask = orangeBallCategory | pavimento2 | asc | blueBallCategory
        nemico2.physicsBody?.contactTestBitMask =  orangeBallCategory | pavimento2 | asc | blueBallCategory
        nemico2.physicsBody?.allowsRotation = false
        nemico2.size = CGSize(width: 200, height: 200)
        nemico2.zPosition = 2
        
        

        
        let textarteff = SKTexture(image: UIImage(named: "Tav")!)
        artefatto1 = SKSpriteNode(texture: textarteff)
        artefatto1.position = CGPoint(x: -700, y: -490)
        artefatto1.name = "art"
        artefatto1.size = CGSize(width: 100, height: 100)
        artefatto1.zPosition = 2
        addChild(artefatto1)
        
         artefatto2 = SKSpriteNode(texture: textarteff)
        artefatto2.position = CGPoint(x: 775, y: -490)
        artefatto2.name = "art2"
        artefatto2.size = CGSize(width: 100, height: 100)
        artefatto2.zPosition = 2
        addChild(artefatto2)
        
        
        artefatto3 = SKSpriteNode(texture: textarteff)
        artefatto3.position = CGPoint(x: -610, y: -70)
        artefatto3.name = "art3"
        artefatto3.size = CGSize(width: 100, height: 100)
        artefatto3.zPosition = 3
        addChild(artefatto3)
        
        
       
        
        artefatto4 = SKSpriteNode(texture: textarteff)
        artefatto4.position = CGPoint(x: -610, y: 268)
        artefatto4.name = "art4"
        artefatto3.size = CGSize(width: 100, height: 100)
        artefatto4.zPosition = 3
        addChild(artefatto4)
        
        
        
        artefatto5 = SKSpriteNode(texture: textarteff)
        artefatto5.position = CGPoint(x: 850, y: 268)
        artefatto5.name = "art5"
        artefatto5.size = CGSize(width: 100, height: 100)
        artefatto5.zPosition = 3
        addChild(artefatto5)
        
        let cuoreText = SKTexture(image: UIImage(named: "cuorePieno")!)
        cuore1 = SKSpriteNode(texture: cuoreText, size: CGSize(width: 30, height: 30))
        cuore1.position = CGPoint(x: 1000, y: -522)
        cuore2 = SKSpriteNode(texture: cuoreText, size: CGSize(width: 30, height: 30))
        cuore2.position = CGPoint(x: 1040, y: -522)
        cuore3 = SKSpriteNode(texture: cuoreText, size: CGSize(width: 30, height: 30))
        cuore3.position = CGPoint(x: 1090, y: -522)
        addChild(cuore1)
        addChild(cuore2)
        addChild(cuore3)
        cuore1.zPosition = 5
        cuore2.zPosition=5
        cuore3.zPosition = 5
        
     shape1.physicsBody?.collisionBitMask = orangeBallCategory | pavimento2 | asc | pavimentosott
      shape1.physicsBody?.categoryBitMask = blueBallCategory
     shape1.physicsBody?.contactTestBitMask =  orangeBallCategory | pavimento2 | asc | pavimentosott
       let path = Bundle.main.path(forResource: "a.flat", ofType:nil)!
              let url = URL(fileURLWithPath: path)

              do {
                
                  Background = try AVAudioPlayer(contentsOf: url)
                  Background.play()
                }
               catch {
                 print("nada")
              }
        
       
     
        
        let fulminetex = SKTexture(image: UIImage(named: "FulminePieno")!)
        fulmine = SKSpriteNode(texture: fulminetex, size: CGSize(width: 80, height: 60))
        fulmine.position = CGPoint(x: 1130, y: -510)
        addChild(fulmine)
        fulmine.zPosition = 5
        
        
        let murosin = SKSpriteNode()
        murosin.size = CGSize(width: 10, height: 1000)
        murosin.position = CGPoint(x: -1200, y: 0)
        addChild(murosin)
        murosin.physicsBody = SKPhysicsBody(rectangleOf: murosin.size)
        murosin.physicsBody?.affectedByGravity = false
        murosin.physicsBody?.isDynamic = false
        murosin.zPosition = 2
        
        
     
        
        let murodes = SKSpriteNode()
        murodes.size = CGSize(width: 10, height: 1000)
        murodes.position = CGPoint(x: 1200, y: 0)
        addChild(murodes)
        murodes.physicsBody = SKPhysicsBody(rectangleOf: murosin.size)
        murodes.physicsBody?.affectedByGravity = false
        murodes.physicsBody?.isDynamic = false
        murodes.zPosition = 2
        
        
        
        nemico3 = SKSpriteNode(texture: nemtext)
         nemico3.physicsBody = SKPhysicsBody(texture: nemtext, size:nemico.frame.size)
        nemico3.physicsBody?.affectedByGravity = true
        nemico3.name = "nemico3"
        nemico3.physicsBody?.categoryBitMask = nemic
        nemico3.physicsBody?.collisionBitMask = blueBallCategory | pavimentosott
        nemico3.physicsBody?.contactTestBitMask = blueBallCategory | pavimentosott
        nemico3.physicsBody?.isDynamic = true
         nemico3.zPosition = 2
         nemico3.position = CGPoint(x: 50, y: -420)
         nemico3.size = CGSize(width: 200, height: 200)
         
         nemico3.physicsBody?.allowsRotation = false
        
        addChild(nemico3)
     
        shape1.position = CGPoint(x: -700, y: -420)
        var timerc = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { timer in
            if(self.m == 0)
            {
                self.m=1
                var s=SKTexture(image: UIImage(named: "w1")!)
                   var s1=SKTexture(image: UIImage(named: "w2")!)
                   var s2=SKTexture(image: UIImage(named: "w3")!)
                   var s3=SKTexture(image: UIImage(named: "w4")!)
                
                
                self.changeTexture = SKAction.animate(with: [s,s1,s2,s3], timePerFrame: 0.2)
                self.nemico3.run(SKAction.repeatForever(self.changeTexture))
            } else if (self.m == 1)
            {
               
                if(self.nemico3.position.x > 330)
                {
                    print("ciao")
                    self.m=2
                }
                else {
                  self.nemico3.position = CGPoint(x: self.nemico3.position.x+10, y: self.nemico3.position.y)
                }
            }else if (self.m == 2)
            {
                self.nemico3.removeAllActions()
                let s = SKTexture(image: UIImage(named: "id1")!)
                                   self.changeTexture = SKAction.animate(with: [s], timePerFrame: 0.2)
                                   self.nemico3.run(self.changeTexture)
                                   self.m=10
                self.timer3 = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: {
                timer in
                  
                    self.m = 3
                })
            }else if self.m==3{
               var s=SKTexture(image: UIImage(named: "w1s")!)
                   var s1=SKTexture(image: UIImage(named: "w2s")!)
                   var s2=SKTexture(image: UIImage(named: "w3s")!)
                   var s3=SKTexture(image: UIImage(named: "w4s")!)
                
                self.m=4
                self.changeTexture = SKAction.animate(with: [s,s1,s2,s3], timePerFrame: 0.2)
                self.nemico3.run(SKAction.repeatForever(self.changeTexture))
                
            }else if (self.m==4)
            {
                 if(self.nemico3.position.x < -280)
                               {
                                   print("ciao")
                                   self.m=5
                               }else {
                    print(self.shape1.position.x - self.nemico3.position.x)
                    if(self.shape1.position.x - self.nemico3.position.x > -270 && self.shape1.position.x < self.nemico3.position.x && self.contatto3 == true && self.shape1.position.y == self.nemico3.position.y)
                               {
                                if(self.mantienitext2 == 0)
                                {
                                    var path = Bundle.main.path(forResource: "Ehi.wav", ofType:nil)!
                                                 let url = URL(fileURLWithPath: path)

                                                 do {
                                                   
                                                    self.NemicoEhi = try AVAudioPlayer(contentsOf: url)
                                                    self.NemicoEhi.play()
                                                   }
                                                  catch {
                                                    print("nada")
                                                 }
                                    
                                    let path1 = Bundle.main.path(forResource: "VistaNemico.flac", ofType:nil)!
                                                                                    let url1 = URL(fileURLWithPath: path1)

                                                                                    do {
                                                                                      
                                                                                        self.NemicoCorsa = try AVAudioPlayer(contentsOf: url1)
                                                                                        self.NemicoCorsa.play()
                                                                                      }
                                                                                     catch {
                                                                                       print("nada")
                                                                                    }
                                    self.mantienitext2 = 1
                                    let s=SKTexture(image: UIImage(named: "run1s")!)
                                       let s1=SKTexture(image: UIImage(named: "run2s")!)
                                       let s2=SKTexture(image: UIImage(named: "run3s")!)
                                       let s3=SKTexture(image: UIImage(named: "run4s")!)
                                      let s4=SKTexture(image: UIImage(named: "run5s")!)
                                    let s5=SKTexture(image: UIImage(named: "run6s")!)
                                    let s6=SKTexture(image: UIImage(named: "run7s")!)
                                    self.changeTexture = SKAction.animate(with: [s,s1,s2,s3,s4,s5,s6], timePerFrame: 0.2)
                                    self.contatto3 = false
                                    self.nemico3.run(SKAction.repeatForever(self.changeTexture))
                                }
                                else{
                                   self.nemico3.position = CGPoint(x: self.nemico3.position.x-20, y: self.nemico3.position.y)
                                }
                                      
                    }else {
                                 self.nemico3.position = CGPoint(x: self.nemico3.position.x-10, y: self.nemico3.position.y)
                        self.mantienitext2 = 0
                    }
                               }
            }else if self.m==5{
                self.nemico3.removeAllActions()
                               let s = SKTexture(image: UIImage(named: "id1ss")!)
                                                  self.changeTexture = SKAction.animate(with: [s], timePerFrame: 0.2)
                                                  self.nemico3.run(self.changeTexture)
                                                  self.m=10
                self.timer4 = Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false, block: {
                               timer in
                                 
                                   self.m = 0
                               })
            }
            
            
            
         
            if(self.shape1.position.x < self.nemico3.position.x && self.mortenemico3 == true)
            {
                
                
                self.mortenemico3 = false
                self.m = 10
                self.stordito3 = 1
                let s = SKTexture(image: UIImage(named: "death1")!)
                let s1 = SKTexture(image: UIImage(named: "death2")!)
                let s2 = SKTexture(image: UIImage(named: "death3")!)
                                   self.timer3.invalidate()
                                   self.timer4.invalidate()
                self.nemico3.physicsBody?.collisionBitMask =  self.pavimentosott
               
                               var changet = SKAction.animate(with: [s,s1,s2], timePerFrame: 0.3)
                                   let s3 = SKTexture(image: UIImage(named: "death4")!)
                                  let s4 = SKTexture(image: UIImage(named: "death5")!)
                                   let s5 = SKTexture(image: UIImage(named: "death6")!)
                
                self.nemico3.run(changet, completion: {
                                        changet = SKAction.animate(with: [s3,s4,s5], timePerFrame: 0.3)
                self.nemico3.run(SKAction.repeatForever(changet))
                    let timera = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false, block: {
                        timer in
                        self.nemico3.removeAllActions()
                        self.m = 3
                        self.nemico3.physicsBody!.collisionBitMask = self.blueBallCategory | self.pavimentosott
                        self.stordito3 = 0
                    })
                                   })
            }
            if(self.shape1.position.x > self.nemico3.position.x && self.mortenemico3 == true)
                      {
                          
                          self.mortenemico3 = false
                          self.m = 10
                        self.stordito3 = 1
                          let s = SKTexture(image: UIImage(named: "death1d")!)
                          let s1 = SKTexture(image: UIImage(named: "death2d")!)
                          let s2 = SKTexture(image: UIImage(named: "death3d")!)
                        self.timer3.invalidate()
                        self.timer4.invalidate()
                           self.nemico3.physicsBody?.collisionBitMask =  self.pavimentosott
                       
                                         var changet = SKAction.animate(with: [s,s1,s2], timePerFrame: 0.3)
                                             let s3 = SKTexture(image: UIImage(named: "death4d")!)
                                            let s4 = SKTexture(image: UIImage(named: "death5d")!)
                                             let s5 = SKTexture(image: UIImage(named: "death6d")!)
                          self.nemico3.run(changet, completion: {
                                                  changet = SKAction.animate(with: [s3,s4,s5], timePerFrame: 0.3)
                          self.nemico3.run(SKAction.repeatForever(changet))
                              let timerb = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false, block: {
                                  timer in
                                  self.nemico3.removeAllActions()
                                  self.m = 0
                                self.nemico3.physicsBody!.collisionBitMask = self.blueBallCategory | self.pavimentosott
                                self.stordito3 = 0
                              })
                                             })
                      }
            if (self.dead){
                if(self.vita > 0)
                {
                    self.dead = false
                    self.contatto3 = false
                    self.shape1.position = CGPoint(x: -300, y: -410)
                }else{
                  
                    if(self.GameOver == false)
                    {
                        self.GameOver = true
                    self.audioPlayer?.play()
                                 self.PlayerArtefatti.stop()
                                 self.PlayerTaser.stop()
                                
                                 let scene = GameScene(fileNamed: "Menu")!
                                 scene.scaleMode = .aspectFill
                                 let transition = SKTransition.moveIn(with: .right, duration: 1)
                                 self.view?.presentScene(scene, transition: transition)
                    print("gameOver")
                    }
                }

            }


            
        })
        
        
        
        
        
        
        
        var timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { timer in
            if(self.g == 0)
            {
                self.g=1
                var s=SKTexture(image: UIImage(named: "w1")!)
                   var s1=SKTexture(image: UIImage(named: "w2")!)
                   var s2=SKTexture(image: UIImage(named: "w3")!)
                   var s3=SKTexture(image: UIImage(named: "w4")!)
                
                
                self.changeTexture = SKAction.animate(with: [s,s1,s2,s3], timePerFrame: 0.2)
                self.nemico2.run(SKAction.repeatForever(self.changeTexture))
            } else if (self.g == 1)
            {
               
                if(self.nemico2.position.x > 300)
                {
                    print("ciao")
                    self.g=2
                }
                else {
                  self.nemico2.position = CGPoint(x: self.nemico2.position.x+10, y: self.nemico2.position.y)
                }
            }else if (self.g == 2)
            {
                self.nemico2.removeAllActions()
                let s = SKTexture(image: UIImage(named: "id1")!)
                                   self.changeTexture = SKAction.animate(with: [s], timePerFrame: 0.2)
                                   self.nemico2.run(self.changeTexture)
                                   self.g=10
                self.timer1 = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: {
                timer in
                  
                    self.g = 3
                })
            }else if self.g==3{
               var s=SKTexture(image: UIImage(named: "w1s")!)
                   var s1=SKTexture(image: UIImage(named: "w2s")!)
                   var s2=SKTexture(image: UIImage(named: "w3s")!)
                   var s3=SKTexture(image: UIImage(named: "w4s")!)
                
                self.g=4
                self.changeTexture = SKAction.animate(with: [s,s1,s2,s3], timePerFrame: 0.2)
                self.nemico2.run(SKAction.repeatForever(self.changeTexture))
                
            }else if (self.g==4)
            {
                 if(self.nemico2.position.x < -200)
                               {
                                   print("ciao")
                                   self.g=5
                               }else {
                    print(self.shape1.position.x - self.nemico2.position.x)
                    if(self.shape1.position.x - self.nemico2.position.x > -270 && self.shape1.position.x < self.nemico2.position.x && self.contatto2 == true)
                               {
                                if(self.mantienitext == 0)
                                {
                                    self.mantienitext = 1
                                    var path = Bundle.main.path(forResource: "Ehi.wav", ofType:nil)!
                                                 let url = URL(fileURLWithPath: path)

                                                 do {
                                                   
                                                    self.NemicoEhi = try AVAudioPlayer(contentsOf: url)
                                                    self.NemicoEhi.play()
                                                   }
                                                  catch {
                                                    print("nada")
                                                 }
                                    
                                    let path1 = Bundle.main.path(forResource: "VistaNemico.flac", ofType:nil)!
                                                                                    let url1 = URL(fileURLWithPath: path1)

                                                                                    do {
                                                                                      
                                                                                        self.NemicoCorsa = try AVAudioPlayer(contentsOf: url1)
                                                                                        self.NemicoCorsa.play()
                                                                                      }
                                                                                     catch {
                                                                                       print("nada")
                                                                                    }
                                    let s=SKTexture(image: UIImage(named: "run1s")!)
                                       let s1=SKTexture(image: UIImage(named: "run2s")!)
                                       let s2=SKTexture(image: UIImage(named: "run3s")!)
                                       let s3=SKTexture(image: UIImage(named: "run4s")!)
                                      let s4=SKTexture(image: UIImage(named: "run5s")!)
                                    let s5=SKTexture(image: UIImage(named: "run6s")!)
                                    let s6=SKTexture(image: UIImage(named: "run7s")!)
                                    self.changeTexture = SKAction.animate(with: [s,s1,s2,s3,s4,s5,s6], timePerFrame: 0.2)
                                    self.nemico2.run(SKAction.repeatForever(self.changeTexture))
                                }
                                else{
                                   self.nemico2.position = CGPoint(x: self.nemico2.position.x-20, y: self.nemico2.position.y)
                                }
                                      
                    }else {
                                 self.nemico2.position = CGPoint(x: self.nemico2.position.x-10, y: self.nemico2.position.y)
                        self.mantienitext = 0
                    }
                               }
            }else if self.g==5{
                self.nemico2.removeAllActions()
                               let s = SKTexture(image: UIImage(named: "id1ss")!)
                                                  self.changeTexture = SKAction.animate(with: [s], timePerFrame: 0.2)
                                                  self.nemico2.run(self.changeTexture)
                                                  self.g=10
                self.timer2 = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: {
                               timer in
                                 
                                   self.g = 0
                               })
            }
            
            
            
         
            if(self.shape1.position.x < self.nemico2.position.x && self.mortenemico2 == true)
            {
                print(self.g)
                self.salvaG = self.g
                self.mortenemico2 = false
                self.g = 10
                self.stordito = 1
                
                let s = SKTexture(image: UIImage(named: "death1")!)
                let s1 = SKTexture(image: UIImage(named: "death2")!)
                let s2 = SKTexture(image: UIImage(named: "death3")!)
                                   self.timer1.invalidate()
                                   self.timer2.invalidate()
                self.nemico2.physicsBody?.collisionBitMask = self.orangeBallCategory | self.pavimento2 | self.asc
                               var changet = SKAction.animate(with: [s,s1,s2], timePerFrame: 0.3)
                                   let s3 = SKTexture(image: UIImage(named: "death4")!)
                                  let s4 = SKTexture(image: UIImage(named: "death5")!)
                                   let s5 = SKTexture(image: UIImage(named: "death6")!)
               
                self.nemico2.run(changet, completion: {
                                        changet = SKAction.animate(with: [s3,s4,s5], timePerFrame: 0.3)
                self.nemico2.run(SKAction.repeatForever(changet))
                    let timer = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false, block: {
                        timer in
                        self.nemico2.removeAllActions()
                        self.g = 3
                        self.nemico2.physicsBody?.collisionBitMask = self.orangeBallCategory | self.pavimento2 | self.asc | self.blueBallCategory
                        self.stordito = 0
                    })
                                   })
            }
            if(self.shape1.position.x > self.nemico2.position.x && self.mortenemico2 == true)
                      {
                          print(self.g)
                          self.salvaG = self.g
                          self.mortenemico2 = false
                          self.g = 10
                        self.stordito = 1
                          let s = SKTexture(image: UIImage(named: "death1d")!)
                          let s1 = SKTexture(image: UIImage(named: "death2d")!)
                          let s2 = SKTexture(image: UIImage(named: "death3d")!)
                        self.timer1.invalidate()
                        self.timer2.invalidate()
                          self.nemico2.physicsBody?.collisionBitMask = self.orangeBallCategory | self.pavimento2 | self.asc
                                         var changet = SKAction.animate(with: [s,s1,s2], timePerFrame: 0.3)
                                             let s3 = SKTexture(image: UIImage(named: "death4d")!)
                                            let s4 = SKTexture(image: UIImage(named: "death5d")!)
                                             let s5 = SKTexture(image: UIImage(named: "death6d")!)
                          self.nemico2.physicsBody?.collisionBitMask = self.orangeBallCategory | self.pavimento2 | self.asc
                          self.nemico2.run(changet, completion: {
                                                  changet = SKAction.animate(with: [s3,s4,s5], timePerFrame: 0.3)
                          self.nemico2.run(SKAction.repeatForever(changet))
                              let timer = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false, block: {
                                  timer in
                                  self.nemico2.removeAllActions()
                                  self.g = 0
                                  self.nemico2.physicsBody?.collisionBitMask = self.orangeBallCategory | self.pavimento2 | self.asc | self.blueBallCategory
                                self.stordito = 0
                              })
                                             })
                      }
            if (self.dead){
                if(self.vita > 0)
                {
                    self.dead = false
                    self.contatto2 = false
                    self.shape1.position = CGPoint(x: 50, y: -410)
                }else{
                    if(self.GameOver == false)
                                       {
                                        self.GameOver = true
                                       self.audioPlayer?.play()
                                                    self.PlayerArtefatti.stop()
                                                    self.PlayerTaser.stop()
                                                   
                                                    let scene = GameScene(fileNamed: "Menu")!
                                                    scene.scaleMode = .aspectFill
                                                    let transition = SKTransition.moveIn(with: .right, duration: 1)
                                                    self.view?.presentScene(scene, transition: transition)
                                       print("gameOver")
                                       }
                }

            }


            
        })
        
        if motionManager.isAccelerometerAvailable{
            motionManager.accelerometerUpdateInterval = 0.2
            motionManager.startAccelerometerUpdates(to: OperationQueue.current!){(data,error)in
                if let mydata = data
                {
                    if(mydata.acceleration.z > 0.1)
                    {
                        self.shake = 1
                    }
                    if(mydata.acceleration.z < -0.1 && self.shake == 1)
                    {
                        if self.ground2.position.y == -130 && self.ascens.position.y == 200
                        {
                            self.ground2.removeAllActions()
                            self.ascens.removeAllActions()
                            let moverec = SKAction.moveTo(y: -555, duration: 1.0)
                           self.ground2.run(moverec)
                            let moveasc = SKAction.moveTo(y: -130, duration: 1.0)
                            self.ascens.run(moveasc)
                            let path = Bundle.main.path(forResource: "elevator.wav", ofType:nil)!
                            let url = URL(fileURLWithPath: path)

                            do {
                              
                                self.Elevator = try AVAudioPlayer(contentsOf: url)
                                self.Elevator.play()
                              }
                             catch {
                               print("nada")
                            }
                        
                        self.shake = 0
                        }else{
                            self.ground2.removeAllActions()
                            self.ascens.removeAllActions()
                            let moverec = SKAction.moveTo(y: -130, duration: 1)
                            let moveasc = SKAction.moveTo(y: 200, duration: 1)
                            self.ascens.run(moveasc)
                            let path = Bundle.main.path(forResource: "elevator.wav", ofType:nil)!
                            let url = URL(fileURLWithPath: path)

                            do {
                              
                                self.Elevator = try AVAudioPlayer(contentsOf: url)
                                self.Elevator.play()
                              }
                             catch {
                               print("nada")
                            }
                            self.ground2.run(moverec) {
                            
                                
                            }
                           
                            self.shake = 0
                            
                            
                        }
                    }
                }
            }
        }
       
    }
    
    var audioPlayer : AVAudioPlayer? {
           get {
               let appDelegate = UIApplication.shared.delegate as! AppDelegate
               return appDelegate.audioPlayer
           }
           set {
               let appDelegate = UIApplication.shared.delegate as! AppDelegate
               appDelegate.audioPlayer = newValue
           }
       }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer?) {
        shape1.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 2.5))
//          shape1.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 0.05))
        btn.isUserInteractionEnabled = true
        btnpressed = true
        
        
    
    }
    override func sceneDidLoad() {
      
       
        
        
    }
    func touchDown(atPoint pos : CGPoint) {
       
    }
    
    func touchMoved(toPoint pos : CGPoint) {
       
    }
    
    func touchUp(atPoint pos : CGPoint) {
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("entro")
        
        shape1.removeAllActions()
        
        b=0
      for touch: AnyObject in touches {
        
        let location = touch.location(in: self)
        let node : SKNode = self.atPoint(location)
        if(artefatto1.alpha == 0 && artefatto2.alpha == 0 && artefatto3.alpha == 0 && artefatto4.alpha == 0 && artefatto5.alpha == 0 && mettiSire == false)
        {
           let test = SKTexture(image: UIImage(named: "SirenaGreen")!)
            sirena = SKSpriteNode(texture: test,size: CGSize(width: 70, height: 70) )
            portaFalsa.size = CGSize(width: 100, height: 100)
            portaFalsa.position = CGPoint(x: 1044, y: 410)
            portaFalsa.physicsBody = SKPhysicsBody(rectangleOf: portaFalsa.size)
            addChild(portaFalsa)
            portaFalsa.name = "porta"
            portaFalsa.physicsBody?.categoryBitMask = 8
            portaFalsa.physicsBody?.collisionBitMask = blueBallCategory
            portaFalsa.physicsBody?.contactTestBitMask = blueBallCategory
            portaFalsa.physicsBody?.isDynamic = false
            portaFalsa.physicsBody?.affectedByGravity = false
                   sirena.position = CGPoint(x: 1044, y: 510)
                   sirena.zPosition = 2
                   addChild(sirena)
        let path = Bundle.main.path(forResource: "elevator.wav", ofType:nil)!
                     let url = URL(fileURLWithPath: path)

                     do {
                       
                        
                       }
                      catch {
                        print("nada")
                     }
            mettiSire = true
        }
        
        
        
        if node.name == "art" {
            
            if(prendiArtEPianoSotto && shape1.position.x < nemico3.position.x){
            let imgtex = SKTexture(image: UIImage(named: "Artefatto 1")!)
            let img = SKSpriteNode(texture: imgtex, size: imgtex.size())
            img.alpha = 0
            let path = Bundle.main.path(forResource: "Paper2.wav", ofType:nil)!
            let url = URL(fileURLWithPath: path)

            do {
              
                PlayerArtefatti = try AVAudioPlayer(contentsOf: url)
                PlayerArtefatti.play()
              }
             catch {
               print("nada")
            }
           addChild(img)
            let change = SKAction.fadeAlpha(to: 1.0, duration: 1.0)
            let change1 = SKAction.fadeAlpha(to: 0.0, duration: 1.0)
            img.run(change)
            img.zPosition = 4
            img.name = "artefatto1"
            node.run(change1)
        }
        }else if let prova = self.childNode(withName: "artefatto1") {
            let fadeout = SKAction.fadeAlpha(to: 0, duration: 1.0)
            prova.run(fadeout, completion: {prova.removeFromParent()})
            }
        
        if node.name == "art2" {
                   
            if(prendiArtEPianoSotto && shape1.position.x > nemico3.position.x){
                   let imgtex = SKTexture(image: UIImage(named: "Artefatto 2")!)
                   let img = SKSpriteNode(texture: imgtex, size: imgtex.size())
                   img.alpha = 0
                   let path = Bundle.main.path(forResource: "Paper2.wav", ofType:nil)!
                                           let url = URL(fileURLWithPath: path)

                                           do {
                                             
                                               PlayerArtefatti = try AVAudioPlayer(contentsOf: url)
                                               PlayerArtefatti.play()
                                             }
                                            catch {
                                              print("nada")
                                           }
                  addChild(img)
                   let change = SKAction.fadeAlpha(to: 1.0, duration: 1.0)
                   let change1 = SKAction.fadeAlpha(to: 0.0, duration: 1.0)
                   img.run(change)
                   img.zPosition = 4
                   img.name = "artefatto2"
                   node.run(change1)
               }
               }else if let prova = self.childNode(withName: "artefatto2") {
                   let fadeout = SKAction.fadeAlpha(to: 0, duration: 1.0)
                   prova.run(fadeout, completion: {prova.removeFromParent()})
                   }
        
        
        if node.name == "art3" {
            
            if(prendiArtPiano1 && shape1.position.x < nemico.position.x){
            let imgtex = SKTexture(image: UIImage(named: "Artefatto 3")!)
            let img = SKSpriteNode(texture: imgtex, size: imgtex.size())
            img.alpha = 0
            let path = Bundle.main.path(forResource: "Paper2.wav", ofType:nil)!
                                    let url = URL(fileURLWithPath: path)

                                    do {
                                      
                                        PlayerArtefatti = try AVAudioPlayer(contentsOf: url)
                                        PlayerArtefatti.play()
                                      }
                                     catch {
                                       print("nada")
                                    }
           addChild(img)
            let change = SKAction.fadeAlpha(to: 1.0, duration: 1.0)
            let change1 = SKAction.fadeAlpha(to: 0.0, duration: 1.0)
            img.run(change)
            img.zPosition = 4
            img.name = "artefatto3"
            node.run(change1)
        }
        }else if let prova = self.childNode(withName: "artefatto3") {
            let fadeout = SKAction.fadeAlpha(to: 0, duration: 1.0)
            prova.run(fadeout, completion: {prova.removeFromParent()})
            }
        
        
        
        
        
        if node.name == "art4" {
            
            if(prendiArtPiano2){
            let imgtex = SKTexture(image: UIImage(named: "Artefatto 4")!)
            let img = SKSpriteNode(texture: imgtex, size: imgtex.size())
            img.alpha = 0
            let path = Bundle.main.path(forResource: "Paper2.wav", ofType:nil)!
                                    let url = URL(fileURLWithPath: path)

                                    do {
                                      
                                        PlayerArtefatti = try AVAudioPlayer(contentsOf: url)
                                        PlayerArtefatti.play()
                                      }
                                     catch {
                                       print("nada")
                                    }
           addChild(img)
            let change = SKAction.fadeAlpha(to: 1.0, duration: 1.0)
            let change1 = SKAction.fadeAlpha(to: 0.0, duration: 1.0)
            img.run(change)
            img.zPosition = 4
            img.name = "artefatto4"
            node.run(change1)
        }
        }else if let prova = self.childNode(withName: "artefatto4") {
            let fadeout = SKAction.fadeAlpha(to: 0, duration: 1.0)
            prova.run(fadeout, completion: {prova.removeFromParent()})
            }
        
        
        
        if node.name == "art5" {
            
            if(prendiArtPiano2 && shape1.position.x > nemico2.position.x){
            let imgtex = SKTexture(image: UIImage(named: "Artefatto 5")!)
            let img = SKSpriteNode(texture: imgtex, size: imgtex.size())
            img.alpha = 0
            let path = Bundle.main.path(forResource: "Paper2.wav", ofType:nil)!
            let url = URL(fileURLWithPath: path)

                                    do {
                                      
                                        PlayerArtefatti = try AVAudioPlayer(contentsOf: url)
                                        PlayerArtefatti.play()
                                      }
                                     catch {
                                       print("nada")
                                    }
           addChild(img)
            let change = SKAction.fadeAlpha(to: 1.0, duration: 1.0)
            let change1 = SKAction.fadeAlpha(to: 0.0, duration: 1.0)
            img.run(change)
            img.zPosition = 4
            img.name = "artefatto5"
            node.run(change1)
        }
        }else if let prova = self.childNode(withName: "artefatto5") {
            let fadeout = SKAction.fadeAlpha(to: 0, duration: 1.0)
            prova.run(fadeout, completion: {prova.removeFromParent()})
            }
        
            
        loc = location
        if (touch.tapCount >= 2)
       {
        var location2 = touch.location(in: self)
             var loc2 = location
        
        if loc2.x < self.frame.midX{
            if(metti == 0){
                taserfunz = true
                addChild(taser)
                metti = 1
                let path = Bundle.main.path(forResource: "Teaser1.aiff", ofType:nil)!
                let url = URL(fileURLWithPath: path)

                do {
                  
                    PlayerTaser = try AVAudioPlayer(contentsOf: url)
                    PlayerTaser.play()
                  }
                 catch {
                   print("nada")
                }
                fulmine.texture = SKTexture(image: UIImage(named: "FulmineVuoto")!)
            shape1.removeAllActions()
            let s=SKTexture(image: UIImage(named: "tasers")!)
            let s1=SKTexture(image: UIImage(named: "taser1s")!)
                let s3=SKTexture(image: UIImage(named: "idle1")!)
            b = 2
                changeTexture = SKAction.animate(with: [s,s1,s3], timePerFrame: 0.4)
            shape1.run(changeTexture)
               
                
                
            
            
            }
        }
        else
        {
            if(metti == 0){
                taserfunz = true
            metti = 1
                
                addChild(taser)
                let path = Bundle.main.path(forResource: "Teaser1.aiff", ofType:nil)!
                               let url = URL(fileURLWithPath: path)

                               do {
                                 
                                   PlayerTaser = try AVAudioPlayer(contentsOf: url)
                                   PlayerTaser.play()
                                 }
                                catch {
                                  print("nada")
                               }
            shape1.removeAllActions()
            let s=SKTexture(image: UIImage(named: "taserd")!)
                       let s1=SKTexture(image: UIImage(named: "taser1d")!)
                 let s2=SKTexture(image: UIImage(named: "idle1d")!)
                      b = 2
                     fulmine.texture = SKTexture(image: UIImage(named: "FulmineVuoto")!)
            changeTexture = SKAction.animate(with: [s,s1,s2], timePerFrame: 0.4)
            shape1.run(changeTexture)
        }
        }
        let timer = Timer.scheduledTimer(withTimeInterval: 0.6, repeats: false, block: {timer in
            self.taserfunz = false
            self.metti = 0
            self.taser.removeFromParent()
            self.b = 0
            self.fulmine.texture = SKTexture(image: UIImage(named: "FulminePieno")!)
            timer.invalidate()
            
            })
        
        }
          //Hold finger at upper area to move character constantly to the right.
          
      }
        if btnpressed == false {
          move = true
        }else{
            move = false
        }
        
        
        
        }

    
    
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    move = false
    print("fine")
    
    if(direzione == false)
    {
        if(b != 2)
       {
        let s1=SKTexture(image: UIImage(named: "idle1d")!)
        
        changeTexture = SKAction.animate(with: [s1], timePerFrame: 0.4)
      shape1.run(SKAction.repeatForever(changeTexture))
        } else{
            b=1
        }
    } else
    {
        if(b != 2)
        {
        let s1=SKTexture(image: UIImage(named: "idle1")!)
        
           changeTexture = SKAction.animate(with: [s1], timePerFrame: 0.4)
        shape1.run(SKAction.repeatForever(changeTexture))
        }else{
            b=1
        }
    }
    
    
        
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
       
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
      if(moveeni == false)
      {
        
        
        if(nemico.position.x-1 < -400)
        {
//            nemico.position = CGPoint(x: nemico.position.x, y: nemico.position.y)
           
            if(c==1)
                      {
                        self.mortenemico = false
                        self.dead = false
                        nemico.removeAllActions()
                       
                            c = 2
                            var s=SKTexture(image: UIImage(named: "id1ss")!)
                             
                             
                          
                          changeTexture = SKAction.animate(with: [s], timePerFrame: 0.5)
                        nemico.run(SKAction.repeatForever(changeTexture))
                        
                         timea = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false, block: { timer in
                            self.moveeni = true
                            self.c = 0
                          
                            timer.invalidate()
                                     })
                       
                        
                      }
            if(mortenemico == true){
                                   if(e == 0)
                                   {
                                    timea.invalidate()
                                   self.nemico.removeAllActions()
                                   e=1
                                   stordito2 = 1
                                   let s = SKTexture(image: UIImage(named: "death1")!)
                                   let s1 = SKTexture(image: UIImage(named: "death2")!)
                                   let s2 = SKTexture(image: UIImage(named: "death3")!)
                                       
                                   nemico.physicsBody?.collisionBitMask = orangeBallCategory | pavimento2 | asc
                                   var changet = SKAction.animate(with: [s,s1,s2], timePerFrame: 0.3)
                                       let s3 = SKTexture(image: UIImage(named: "death4")!)
                                      let s4 = SKTexture(image: UIImage(named: "death5")!)
                                       let s5 = SKTexture(image: UIImage(named: "death6")!)
                                       nemico.run(changet, completion: {
                                            changet = SKAction.animate(with: [s3,s4,s5], timePerFrame: 0.3)
                                           self.nemico.run(SKAction.repeatForever(changet))
                                       })
                                   
                                       let timer = Timer.scheduledTimer(withTimeInterval: 10.0, repeats: false, block: { timer in
                                        
                                           self.mortenemico = false
                                           self.e=0
                                           self.c = 0
                                        self.stordito2 = 0
                                          self.moveeni = true
                                        
                                           changet = SKAction.animate(with: [s3,s2,s1,s], timePerFrame: 0.3)
                                           self.nemico.run(changet)
                                           self.nemico.physicsBody?.collisionBitMask = self.orangeBallCategory | self.pavimento2 | self.asc | self.blueBallCategory
                                           self.dead = false
                                           
                                       })
                                   }
                                   
                               }
            else if (dead){
                if(quellosopra == false){
                               if(vita > 0)
                               {
                                dead = false
                               shape1.position = CGPoint(x: -300, y: -410)
                               }else{    if(self.GameOver == false)
                                                  {
                                                    self.GameOver = true
                                                  self.audioPlayer?.play()
                                                               self.PlayerArtefatti.stop()
                                                               self.PlayerTaser.stop()
                                                              
                                                               let scene = GameScene(fileNamed: "Menu")!
                                                               scene.scaleMode = .aspectFill
                                                               let transition = SKTransition.moveIn(with: .right, duration: 1)
                                                               self.view?.presentScene(scene, transition: transition)
                                                  print("gameOver")
                                                  }
                               }
                               }

            }
           
            
            
        }
        else
        {
            
            if(c==0 && e==0)
            {
                d=0
                nemico.removeAllActions()
                c=1
                var s=SKTexture(image: UIImage(named: "w1s")!)
                var s1=SKTexture(image: UIImage(named: "w2s")!)
                var s2=SKTexture(image: UIImage(named: "w3s")!)
                var s3=SKTexture(image: UIImage(named: "w4s")!)
                  
                
                changeTexture = SKAction.animate(with: [s,s1,s2,s3], timePerFrame: 0.2)
                nemico.run(SKAction.repeatForever(changeTexture))
               
                
                
            }
//            print(nemico.frame.origin.x - shape1.frame.origin.x)
            if(nemico.frame.origin.x - shape1.frame.origin.x  < 122 && contatto == true && mortenemico == false && shape1.frame.origin.x < nemico.frame.origin.x)
            {
                
                if(!dead && e==0){
                if(d == 0)
                {
                    d = 1
                    var path = Bundle.main.path(forResource: "Ehi.wav", ofType:nil)!
                                 let url = URL(fileURLWithPath: path)

                                 do {
                                   
                                    self.NemicoEhi = try AVAudioPlayer(contentsOf: url)
                                    self.NemicoEhi.play()
                                   }
                                  catch {
                                    print("nada")
                                 }
                    
                    let path1 = Bundle.main.path(forResource: "VistaNemico.flac", ofType:nil)!
                                                                    let url1 = URL(fileURLWithPath: path1)

                                                                    do {
                                                                      
                                                                        self.NemicoCorsa = try AVAudioPlayer(contentsOf: url1)
                                                                        self.NemicoCorsa.play()
                                                                      }
                                                                     catch {
                                                                       print("nada")
                                                                    }
                    let s=SKTexture(image: UIImage(named: "run1s")!)
                       let s1=SKTexture(image: UIImage(named: "run2s")!)
                       let s2=SKTexture(image: UIImage(named: "run3s")!)
                       let s3=SKTexture(image: UIImage(named: "run4s")!)
                      let s4=SKTexture(image: UIImage(named: "run5s")!)
                    let s5=SKTexture(image: UIImage(named: "run6s")!)
                    let s6=SKTexture(image: UIImage(named: "run7s")!)
                    changeTexture = SKAction.animate(with: [s,s1,s2,s3,s4,s5,s6], timePerFrame: 0.2)
                    nemico.run(SKAction.repeatForever(changeTexture))
                    }
                    
                nemico.position = CGPoint(x: nemico.position.x-5, y: nemico.position.y)
                    
                }
                else{
                    
                    print(vita)
                    
                     if(quellosopra == false){
                                   if(vita > 0)
                                   {
                                    dead = false
                                    shape1.position = CGPoint(x: -300, y: -410)
                                   }else{
                                    if(self.GameOver == false)
                                                       {
                                                        self.GameOver = true
                                                       self.audioPlayer?.play()
                                                                    self.PlayerArtefatti.stop()
                                                                    self.PlayerTaser.stop()
                                                                   
                                                                    let scene = GameScene(fileNamed: "Menu")!
                                                                    scene.scaleMode = .aspectFill
                                                                    let transition = SKTransition.moveIn(with: .right, duration: 1)
                                                                    self.view?.presentScene(scene, transition: transition)
                                                       print("gameOver")
                                                       }
                                   }
                                   }
                }
            }
            else if(mortenemico == true){
                if(e == 0)
                {
                self.nemico.removeAllActions()
                e=1
                stordito2 = 1
                let s = SKTexture(image: UIImage(named: "death1")!)
                let s1 = SKTexture(image: UIImage(named: "death2")!)
                let s2 = SKTexture(image: UIImage(named: "death3")!)
                    
                nemico.physicsBody?.collisionBitMask = orangeBallCategory | pavimento2 | asc
                var changet = SKAction.animate(with: [s,s1,s2], timePerFrame: 0.3)
                    let s3 = SKTexture(image: UIImage(named: "death4")!)
                   let s4 = SKTexture(image: UIImage(named: "death5")!)
                    let s5 = SKTexture(image: UIImage(named: "death6")!)
                    nemico.run(changet, completion: {
                         changet = SKAction.animate(with: [s3,s4,s5], timePerFrame: 0.3)
                        self.nemico.run(SKAction.repeatForever(changet))
                    })
                
                    let timer = Timer.scheduledTimer(withTimeInterval: 10.0, repeats: false, block: { timer in
                        self.mortenemico = false
                        self.e=0
                        self.c = 0
                        self.stordito2 = 0
                        changet = SKAction.animate(with: [s3,s2,s1,s], timePerFrame: 0.3)
                        self.nemico.run(changet)
                        self.nemico.physicsBody?.collisionBitMask = self.orangeBallCategory | self.pavimento2 | self.asc | self.blueBallCategory
                        self.dead = false
                        
                    })
                }
                
            }else{
                if(e == 0)
                {
                  nemico.position = CGPoint(x: nemico.position.x-3, y: nemico.position.y)
                    
                }
            }
        }
      }else{
        if(nemico.position.x > 200
            )
                {
        //            nemico.position = CGPoint(x: nemico.position.x, y: nemico.position.y)
                    if(c==1)
                              {
                                d=0
                                nemico.removeAllActions()
                               c = 2
                                  var s=SKTexture(image: UIImage(named: "id1")!)
                                    
                                     
                                  
                                  changeTexture = SKAction.animate(with: [s], timePerFrame: 0.5)
                                nemico.run(SKAction.repeatForever(changeTexture))
                                s=SKTexture(image: UIImage(named: "id1ss")!)
                                    
                                     
                                  
                                  changeTexture = SKAction.animate(with: [s], timePerFrame: 0.5)
                                
                                  timea = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false, block: { timer in
                                    self.moveeni = false
                                    self.c = 0
                                    
                                                 timer.invalidate()
                                             })
                                timea1 = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false, block: { timer in
                                self.moveeni = false
                                self.c = 0
                                
                                             timer.invalidate()
                                         })
                              }
                   
                   else if(mortenemico == true){
                        if(e == 0)
                        {
                            
                            timea.invalidate()
                        self.nemico.removeAllActions()
                        e=1
                       
                        let s = SKTexture(image: UIImage(named: "death1d")!)
                        let s1 = SKTexture(image: UIImage(named: "death2d")!)
                        let s2 = SKTexture(image: UIImage(named: "death3d")!)
                        stordito2 = 1
                        nemico.physicsBody?.collisionBitMask = orangeBallCategory | pavimento2 | asc
                        var changet = SKAction.animate(with: [s,s1,s2], timePerFrame: 0.3)
                            let s3 = SKTexture(image: UIImage(named: "death4d")!)
                           let s4 = SKTexture(image: UIImage(named: "death5d")!)
                            let s5 = SKTexture(image: UIImage(named: "death6d")!)
                            nemico.run(changet, completion: {
                                 changet = SKAction.animate(with: [s3,s4,s5], timePerFrame: 0.3)
                                self.nemico.run(SKAction.repeatForever(changet))
                            })
                        
                            let timer = Timer.scheduledTimer(withTimeInterval: 10.0, repeats: false, block: { timer in
                                self.mortenemico = false
                                self.e=0
                                self.c = 0
                                self.moveeni = false
                                self.stordito2 = 0
                                changet = SKAction.animate(with: [s3,s2,s1,s], timePerFrame: 0.3)
                                self.nemico.run(changet)
                                self.nemico.physicsBody?.collisionBitMask = self.orangeBallCategory | self.pavimento2 | self.asc | self.blueBallCategory
                                self.dead = false
                                
                            })
                        }
                        
                    }else if (dead){
                        if(quellosopra == false){
                                       if(vita > 0)
                                       {
                                        dead = false
                                       shape1.position = CGPoint(x: -300, y: -410)
                                       }else{
                                        if(self.GameOver == false)
                                                           {
                                                            self.GameOver = true
                                                           self.audioPlayer?.play()
                                                                        self.PlayerArtefatti.stop()
                                                                        self.PlayerTaser.stop()
                                                                       
                                                                        let scene = GameScene(fileNamed: "Menu")!
                                                                        scene.scaleMode = .aspectFill
                                                                        let transition = SKTransition.moveIn(with: .right, duration: 1)
                                                                        self.view?.presentScene(scene, transition: transition)
                                                           print("gameOver")
                                                           }
                                       }
                                       }

                    }
                    
                    
                   
                    
                    
        }else{
       
        if(c==0)
                   {
                    nemico.removeAllActions()
                       c=1
                       var s=SKTexture(image: UIImage(named: "w1")!)
                          var s1=SKTexture(image: UIImage(named: "w2")!)
                          var s2=SKTexture(image: UIImage(named: "w3")!)
                          var s3=SKTexture(image: UIImage(named: "w4")!)
                       
                       
                       changeTexture = SKAction.animate(with: [s,s1,s2,s3], timePerFrame: 0.2)
                       nemico.run(SKAction.repeatForever(changeTexture))
               
                   }
                  
            
       
            print(nemico.frame.origin.x - shape1.frame.origin.x)
           print (contatto)
            if(nemico.frame.origin.x - shape1.frame.origin.x  > -140 && contatto == true && mortenemico == false && shape1.frame.origin.x > nemico.frame.origin.x)
        {
            print (dead)
            print(e)
            if(!dead && e==0){
            if(d == 0)
            {
                d = 1
                var path = Bundle.main.path(forResource: "Ehi.wav", ofType:nil)!
                             let url = URL(fileURLWithPath: path)

                             do {
                               
                                self.NemicoEhi = try AVAudioPlayer(contentsOf: url)
                                self.NemicoEhi.play()
                               }
                              catch {
                                print("nada")
                             }
                
                let path1 = Bundle.main.path(forResource: "VistaNemico.flac", ofType:nil)!
                                                                let url1 = URL(fileURLWithPath: path1)

                                                                do {
                                                                  
                                                                    self.NemicoCorsa = try AVAudioPlayer(contentsOf: url1)
                                                                    self.NemicoCorsa.play()
                                                                  }
                                                                 catch {
                                                                   print("nada")
                                                                }
                let s=SKTexture(image: UIImage(named: "run1")!)
                   let s1=SKTexture(image: UIImage(named: "run2")!)
                   let s2=SKTexture(image: UIImage(named: "run3")!)
                   let s3=SKTexture(image: UIImage(named: "run4")!)
                  let s4=SKTexture(image: UIImage(named: "run5")!)
                let s5=SKTexture(image: UIImage(named: "run6")!)
                let s6=SKTexture(image: UIImage(named: "run7")!)
                changeTexture = SKAction.animate(with: [s,s1,s2,s3,s4,s5,s6], timePerFrame: 0.2)
                nemico.run(SKAction.repeatForever(changeTexture))
                }
                
            nemico.position = CGPoint(x: nemico.position.x+5, y: nemico.position.y)
                
            }
            else if(e==0){
                
                print(vita)
                if(quellosopra == false){
                if(vita > 0)
                {
                 dead = false
                shape1.position = CGPoint(x: -300, y: -410)
                }else{
                    if(self.GameOver == false)
                                       {
                                        self.GameOver = true
                                       self.audioPlayer?.play()
                                                    self.PlayerArtefatti.stop()
                                                    self.PlayerTaser.stop()
                                                   
                                                    let scene = GameScene(fileNamed: "Menu")!
                                                    scene.scaleMode = .aspectFill
                                                    let transition = SKTransition.moveIn(with: .right, duration: 1)
                                                    self.view?.presentScene(scene, transition: transition)
                                       print("gameOver")
                                       }
                }
                }
            }
            
        }else if(mortenemico == true){
            if(e == 0)
            {
            self.nemico.removeAllActions()
            e=1
            let s = SKTexture(image: UIImage(named: "death1d")!)
            let s1 = SKTexture(image: UIImage(named: "death2d")!)
            let s2 = SKTexture(image: UIImage(named: "death3d")!)
            stordito2 = 1
            nemico.physicsBody?.collisionBitMask = orangeBallCategory | pavimento2 | asc
            var changet = SKAction.animate(with: [s,s1,s2], timePerFrame: 0.3)
            nemico.run(changet)
            changet = SKAction.animate(with: [s,s1,s2], timePerFrame: 0.3)
                                           let s3 = SKTexture(image: UIImage(named: "death4d")!)
                                          let s4 = SKTexture(image: UIImage(named: "death5d")!)
                                           let s5 = SKTexture(image: UIImage(named: "death6d")!)
                                           nemico.run(changet, completion: {
                                                changet = SKAction.animate(with: [s3,s4,s5], timePerFrame: 0.3)
                                               self.nemico.run(SKAction.repeatForever(changet))
                                           })
                let timer = Timer.scheduledTimer(withTimeInterval: 10.0, repeats: false, block: { timer in
                    self.mortenemico = false
                    self.e=0
                    self.c = 0
                    self.stordito2 = 0
                    changet = SKAction.animate(with: [s3,s2,s1,s], timePerFrame: 0.3)
                    self.nemico.run(changet)
                    self.nemico.physicsBody?.collisionBitMask = self.orangeBallCategory | self.pavimento2 | self.asc | self.blueBallCategory
                    self.dead = false
                    
                })
        
        
        }
        }else{
        if(e == 0)
        {
          nemico.position = CGPoint(x: nemico.position.x+3, y: nemico.position.y)
            
        }
        }
        }
        }
        if (move == true)
        {
            if loc.y > 400{
                 
             

             }else{
            
               
               if loc.x < self.frame.midX{
               
                shape1.position = CGPoint(x: shape1.position.x-6, y: shape1.position.y)
                taser.position = CGPoint(x: shape1.position.x-110, y: shape1.frame.midY+20)
                
               direzione = true
               
                
                 if(loc.y > 100) {
                 
//                   shape1.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 0.50))
                    
                     print("vado1")
                   
                 }

               } else if loc.x > self.frame.midX{
               shape1.position = CGPoint(x: shape1.position.x+6, y: shape1.position.y)
                 taser.position = CGPoint(x: shape1.position.x+110, y: shape1.frame.midY+20)
                direzione = false
                 

              
                 if(loc.y > 100) {
//                    shape1.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 0.05))
                   
                 }
             }
        }
            if(b == 0)
                   {
                   b=1
                     shape1.removeAllActions()
                    if (direzione == false)
                    {
                   
                       let s=SKTexture(image: UIImage(named: "idle1d")!)
                       let s1=SKTexture(image: UIImage(named: "spd")!)
                       let s2=SKTexture(image: UIImage(named: "idle2d-1")!)
                    
                        changeTexture = SKAction.animate(with: [s,s1,s2], timePerFrame: 0.1)
                    }else
                    {
                        let s=SKTexture(image: UIImage(named: "idle1")!)
                        let s1=SKTexture(image: UIImage(named: "sps")!)
                        let s2=SKTexture(image: UIImage(named: "idle2d-2")!)
                        
                    
                         changeTexture = SKAction.animate(with: [s,s1,s2], timePerFrame: 0.1)
                    }
                        shape1.run(changeTexture, completion: {
                            
                            self.b=0
                            
                            })
                    
                       }
    }
        
       
            
           
        
    
               
}
    func didBegin(_ contact: SKPhysicsContact) {
        print(contact.bodyA.node!.name)
        print(contact.bodyB.node!.name)
        if(contact.bodyA.node!.name == "pavimento" && contact.bodyB.node!.name=="player" )
        {
        contatto = true
        prendiArtEPianoSotto = false
        prendiArtPiano1 = true
        prendiArtPiano2 = true
        
        }
        if(contact.bodyA.node!.name == "pavimento2" && contact.bodyB.node!.name=="player")
        {
            contatto2 = true
         prendiArtEPianoSotto = false
            prendiArtPiano1 = true
            prendiArtPiano2 = true
            contatto = false
        }
        if(contact.bodyA.node!.name == "pavimentosotto" && contact.bodyB.node!.name=="player")
        {
            contatto2 = false
            contatto = false
            prendiArtEPianoSotto = true
            prendiArtPiano1 = true
            prendiArtPiano2 = true
        }
        if contact.bodyA.node!.name == "nemico" && contact.bodyB.node!.name=="player" && stordito2 == 0
        {
            
                      if(ancoraflag == 0)
                      {
                        let cgange = SKAction.fadeAlpha(to: 1.0, duration: 1.0)
                        artefatto1.run(cgange)
                        artefatto2.run(cgange)
                        artefatto3.run(cgange)
                        artefatto4.run(cgange)
                        artefatto5.run(cgange)
                        sirena.removeFromParent()
                        mettiSire = false
                      dead = true
                      vita-=1
                           shape1.position = CGPoint(x: -700, y: -280)
                        if(vita == 2)
                        {
                            cuore3.texture = SKTexture(image: UIImage(named: "CuoreVuoto")!)
                        }else if vita == 1{
                            cuore2.texture = SKTexture(image: UIImage(named: "CuoreVuoto")!)
                        } else{
                            cuore1.texture = SKTexture(image: UIImage(named: "CuoreVuoto")!)
                            }
                        
                      mortenemico = false
                      ancoraflag+=1
                        let timer = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: {
                                                         timer in
                                      self.ancoraflag = 0
                                                     })
                      }
            }
        
        
        
        
        if(contact.bodyA.node!.name == "nemico" && contact.bodyB.node!.name=="taser" && taserfunz == false)
        {
           if (e == 0)
            {
            if(ancoraflag == 0)
            {
            dead = true
            vita-=1
            
            mortenemico = false
            ancoraflag+=1
            let timer = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: {
                                   timer in
                self.ancoraflag = 0
                               })
            }
                      }
        }
        else if(contact.bodyA.node!.name == "nemico" && contact.bodyB.node!.name=="taser" && taserfunz == true)
        {
            dead = false
            mortenemico = true
        }
        
        if(contact.bodyA.node!.name == "player" && contact.bodyB.node!.name=="nemico2" && taserfunz == false && stordito == 0)
            
              {
                quellosopra = true
                if(ancoraflag == 0)
                  {
                    let cgange = SKAction.fadeAlpha(to: 1.0, duration: 1.0)
                    artefatto1.run(cgange)
                    artefatto2.run(cgange)
                    artefatto3.run(cgange)
                    artefatto4.run(cgange)
                    artefatto5.run(cgange)
                    sirena.removeFromParent()
                    portaFalsa.removeFromParent()
                    mettiSire = false
                  dead = true
                  vita-=1
                    if(vita == 2)
                                           {
                                               cuore3.texture = SKTexture(image: UIImage(named: "CuoreVuoto")!)
                                           }else if vita == 1{
                                               cuore2.texture = SKTexture(image: UIImage(named: "CuoreVuoto")!)
                                           } else{
                                               cuore1.texture = SKTexture(image: UIImage(named: "CuoreVuoto")!)
                                               }
                       shape1.position = CGPoint(x: -700, y: -280)
                  mortenemico2 = false
                  ancoraflag+=1
                    let timer = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: {
                        timer in
                        self.ancoraflag = 0
                        self.quellosopra = false
                    })
                }
        }
              
              else if(contact.bodyA.node!.name == "nemico2" && contact.bodyB.node!.name=="taser" && taserfunz == true)
              {
                  dead = false
                  mortenemico2 = true
              }
        
        
        if(contact.bodyA.node!.name == "player" && contact.bodyB.node!.name=="nemico3" && taserfunz == false && stordito3 == 0)
            
              {
                quellosopra = true
                if(ancoraflag == 0)
                  {
                    let cgange = SKAction.fadeAlpha(to: 1.0, duration: 1.0)
                    artefatto1.run(cgange)
                    artefatto2.run(cgange)
                    artefatto3.run(cgange)
                    artefatto4.run(cgange)
                    artefatto5.run(cgange)
                    portaFalsa.removeFromParent()
                    sirena.removeFromParent()
                    mettiSire = false
                  dead = true
                  vita-=1
                    if(vita == 2)
                                           {
                                               cuore3.texture = SKTexture(image: UIImage(named: "CuoreVuoto")!)
                                           }else if vita == 1{
                                               cuore2.texture = SKTexture(image: UIImage(named: "CuoreVuoto")!)
                                           } else{
                                               cuore1.texture = SKTexture(image: UIImage(named: "CuoreVuoto")!)
                                               }
                         shape1.position = CGPoint(x: -700, y: -280)
                  mortenemico3 = false
                  ancoraflag+=1
                    let timer = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: {
                        timer in
                        self.ancoraflag = 0
                        self.quellosopra = false
                    })
                }
        }
        
        else if(contact.bodyA.node!.name == "nemico3" && contact.bodyB.node!.name=="taser" && taserfunz == true)
        {
            mortenemico3 = true
            dead = false
            contatto3 = true
        }
        
        if (contact.bodyA.node!.name == "ascensore" && contact.bodyB.node!.name=="player" )
        {
            
        }
        if (contact.bodyA.node!.name == "player" && contact.bodyB.node!.name=="porta" )
               {
                self.removeAllActions()
                self.removeAllChildren()
                NemicoEhi.stop()
                NemicoCorsa.stop()
                PlayerTaser.stop()
                   let scene = GameScene(fileNamed: "Livello2")!
                                  scene.scaleMode = .aspectFill
                                  let transition = SKTransition.moveIn(with: .right, duration: 1)
                                  self.view?.presentScene(scene, transition: transition)
               }

    }
}
